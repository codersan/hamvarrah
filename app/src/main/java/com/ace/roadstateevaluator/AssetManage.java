package com.ace.roadstateevaluator;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.HapticFeedbackConstants;
import android.view.View;


public class AssetManage {

//----------------------------
    public AssetManage(){
    }

    public String MakeArabic(String s){
        return s.replaceAll("0","۰")
                .replaceAll("1","۱")
                .replaceAll("2","۲")
                .replaceAll("3","۳")
                .replaceAll("4","۴")
                .replaceAll("5","۵")
                .replaceAll("6","۶")
                .replaceAll("7","۷")
                .replaceAll("8","۸")
                .replaceAll("9","۹");
    }


    public void Haptic(View view) {
            view.setHapticFeedbackEnabled(true);
            view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
    }
        public void HapticLong(View view){
                view.setHapticFeedbackEnabled(true);
                view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS , HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);

    }
}
