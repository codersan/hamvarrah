package com.ace.roadstateevaluator.DB;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.GsonModels.Car;

//--------------------------------------------
public class CarDB extends SQLiteOpenHelper {
    Car car0;
    static final String DB_NAME = "cars-db";
    final String query="CREATE TABLE IF NOT EXISTS'"+"tb_cars"+"'('"+
            Car.KeyId + "' TEXT, '" +
            Car.KeyName + "' TEXT, '" +
            Car.KeyCreation + "' TEXT, '" +
            Car.KeyUsage + "' TEXT, '" +
            Car.Keylength + "' TEXT, '" +
            Car.KeySelect + "' TEXT" + ")";;

    //--------------------------------------------

    public CarDB(Context context ) {
        super(context, DB_NAME, null, InitActivity.getSqliteInt());

    }
    //--------------------------------------------
    @Override
    //onCreate() is only run when the database file did not exist and was just created.
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
        Log.d("sqlllsms", "created");

    }
    //--------------------------------------------
    @Override
    // onUpgrade() is only called when the database file exists but the stored version number is lower than requested in constructor.
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        InitActivity.getSqliteInt();
    }
    //--------------------------------------------
    public void Insert(Car car) {
        car0=car;
        ContentValues value = new ContentValues();
        value.put(Car.KeyId, car0.getCarid()+"");
        value.put(Car.KeyName, car0.getCarname().trim());
        value.put(Car.KeyCreation, car0.getCreatyear()+"");
        value.put(Car.KeyUsage, car0.getUsagekm()+"");
        value.put(Car.Keylength, car0.getCarlength()+"");
        value.put(Car.KeySelect, car0.getSelected()+"");
        SQLiteDatabase db = getWritableDatabase();
        //-1 return means canot insert into table
        Long insertid = db.insert("tb_cars", null, value);
        Log.d("sqlllsms", ("insert id =" + insertid));
        if (db.isOpen()) db.close();
    }
    //--------------------------------------------
    public  void UpdateDB(Car car) {
        car0=car;
        ContentValues value = new ContentValues();
        value.put(Car.KeyId, car0.getCarid()+"");
        value.put(Car.KeyName, car0.getCarname().trim());
        value.put(Car.KeyCreation, car0.getCreatyear()+"");
        value.put(Car.KeyUsage, car0.getUsagekm()+"");
        value.put(Car.Keylength, car0.getCarlength()+"");
        value.put(Car.KeySelect, car0.getSelected()+"");
        SQLiteDatabase db = getWritableDatabase();
        int count = db.update("tb_cars" , value , car0.KeyId +" ='"+car0.getCarid()+"'"  , null); //count return how many rows
        Log.d("sqlllsms" , "row updated :"+count);
        if(db.isOpen())db.close();
    }
    //--------------------------------------------
    public void delete(){ // for delete all rows

        SQLiteDatabase db = getWritableDatabase();
        String table;
        int count = db.delete("tb_cars" , null, null);
        Log.i("sqllllimit" , "remover row");
        if(db.isOpen())db.close();
    }
}



