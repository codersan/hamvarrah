package com.ace.roadstateevaluator.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.GsonModels.Car;
import com.ace.roadstateevaluator.GsonModels.GetTakeResponse;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;

//--------------------------------------------
public class TakeDB extends SQLiteOpenHelper {

    public static String KeyDate="date";
    public static String KeyModel="carmodel";
    public static String KeyLength="totallength";
    public static String KeyDesc="description";
    static final String DB_NAME = "takes-db";
    final String query="CREATE TABLE IF NOT EXISTS'"+"tb_takes"+"'('"+
            KeyDate + "' TEXT, '" +
            KeyModel + "' TEXT, '" +
            KeyLength + "' TEXT, '" +
            KeyDesc + "' TEXT" + ")";

    //--------------------------------------------

    public TakeDB(Context context ) {
        super(context, DB_NAME, null, InitActivity.getSqliteInt());

    }
    //--------------------------------------------
    @Override
    //onCreate() is only run when the database file did not exist and was just created.
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
        Log.d("sqlllsms", "created");

    }
    //--------------------------------------------
    @Override
    // onUpgrade() is only called when the database file exists but the stored version number is lower than requested in constructor.
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        InitActivity.getSqliteInt();
    }
    //--------------------------------------------
    public void Insert(GetTakeResponse takeResponse) {
        Log.i("sqllllimit1" , takeResponse.getDesc()+takeResponse.getStart());
        ContentValues value = new ContentValues();
        value.put(KeyDate, takeResponse.getStart());
        value.put(KeyModel, GetUserCarsList.getNameAndId().get(takeResponse.getCarid()));
        value.put(KeyLength, takeResponse.getTotallen()+"");
        value.put(KeyDesc, takeResponse.getDesc());
        SQLiteDatabase db = getWritableDatabase();
        //-1 return means canot insert into table
        Long insertid = db.insert("tb_takes", null, value);
         Log.d("sqllllimit", ("insert id =" + insertid)+takeResponse.getStart()+"--"+GetUserCarsList.getNameAndId().get(takeResponse.getCarid())+"--"+takeResponse.getDesc());
        if (db.isOpen()) db.close();
    }
    //--------------------------------------------

    //--------------------------------------------
    public void delete(){ // for delete all rows

        SQLiteDatabase db = getWritableDatabase();
        String table;
        int count = db.delete("tb_takes" , null, null);
        //Log.i("sqllllimit" , "remover row");
        if(db.isOpen())db.close();
    }

    private class InsertHeavy extends AsyncTask<String, String, String> {
       public GetTakeResponse take0;
        @Override
        protected String doInBackground(String... strings) {
            //to create a model for values we need to inseert use following
            //can create a method getcontentvalue in flower also for better coding!

            return null;
        }
    }
}



