package com.ace.roadstateevaluator;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.text.Edits;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.FragmentActivity;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.LoginActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;

import com.ace.roadstateevaluator.DB.CarDB;
import com.ace.roadstateevaluator.DB.TakeDB;
import com.ace.roadstateevaluator.GsonModels.Car;
import com.ace.roadstateevaluator.GsonModels.CarModels;
import com.ace.roadstateevaluator.GsonModels.CompletePacket;
import com.ace.roadstateevaluator.GsonModels.GetOrganList;
import com.ace.roadstateevaluator.GsonModels.GetServerCarsModel;
import com.ace.roadstateevaluator.GsonModels.GetTakeListForFuck;
import com.ace.roadstateevaluator.GsonModels.GetTakeResponse;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.GsonModels.GetTakeList;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsListForFuckEnd;
import com.ace.roadstateevaluator.GsonModels.LoginResponse;
import com.ace.roadstateevaluator.GsonModels.RefreshResponse;
import com.ace.roadstateevaluator.GsonModels.User;
import com.ace.roadstateevaluator.GsonModels.UserResponse;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyUtiles {

    public static final Map<String, String> EnglishToPersian = new HashMap<String, String>() {{

        put("bsc", "کارشناسی");
        put("msc", "کارشناسی ارشد");
        put("phd", "دکتری"); //student level
        put("project", "پروژه درسی");
        put("paper", "مقاله علمی");
        put("thesis", "پایان نامه");
        put("others", "سایر"); //target use for student and prof but prof dont have project
        put("instructor", "مربی");
        put("assistant_professor", "استادیار");
        put("associate_professor", "دانشیار");
        put("full_professor", "استاد");
        put("Head of the organization", "مسئول سازمان");
        put("Head of Project", "مسئول پروژه");
        put("Data collector", "برداشت کننده داده");
    }}; //prof level

    public static final Map<String, String> PersianToEnglish = new HashMap<String, String>() {{
        put("کارشناسی", "bsc");
        put("کارشناسی ارشد", "msc");
        put("دکتری", "phd"); //student level
        put("پروژه درسی", "project");
        put("مقاله علمی", "paper");
        put("پایان نامه", "thesis");
        put("سایر", "others"); //target use for student and prof but prof dont have project
        put("مربی", "instructor");
        put("استادیار", "assistant_professor");
        put("دانشیار", "associate_professor");
        put("استاد", "full_professor");
        put("مسئول سازمان", "Head of the organization");
        put("مسئول پروژه", "Head of Project");
        put("برداشت کننده داده", "Data collector");
    }}; //prof level

    public static final Map<String, String> USER_TYPE_p2e = new HashMap<String, String>(){{
        put("عادی", "regular");
        put("دانشجو", "student");
        put("هیات علمی", "professor");
        put("سازمانی", "project");
    }};
    public static final Map<String, String> USER_TYPE_e2p = new HashMap<String, String>(){{
        put("regular", "عادی");
        put("student", "دانشجو");
        put("professor", "هیات علمی");
        put("project", "سازمانی");
    }};
    //----------------------------------------------
    public static boolean CheckEmail(String email) {
        String regex = "^(.+)@(.+)[.](.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        System.out.println(email + " : " + matcher.matches());
        return matcher.matches();
    }

    public static boolean CheckPhone(String email) {
        if (email.length() == 11 & email.startsWith("09")) {
            return true;
        } else {
            return false;
        }
    }

    //for a digit number id
    public static String IdCreator(int a) {
        Random r = new Random();
        String s = "";
        for (int i = 0; i < a; i++) {
            s = s + r.nextInt(9);
        }
        return s;
    }

    public static String GetDate() {
        DateFormat df = new SimpleDateFormat("yyyy:MM:dd");
        Date datenow = new Date();
        String s = df.format(datenow).replace(":", "-");
        return s;
    }

    public static String GetTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public static String GetSHA(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        // Static getInstance method is called with hashing SHA
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        // to calculate message digest of an input
        // and return array of byte
        byte[] bytes = md.digest(input.getBytes("UTF-8"));

        // Convert byte array into signum representation
        BigInteger number = new BigInteger(1, bytes);
        // Convert message digest into hex value
        StringBuilder hexString = new StringBuilder(number.toString(16));
        // Pad with leading zeros
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }

    public static void ListFilesAndPost(Context context) {
        if (isNetworkConnected(context)) {
            File root = new File(context.getFilesDir().getAbsolutePath());
            File files[] = root.listFiles();
            for (final File f : files) {
                if (f.getName().endsWith(".json") && f.getName().startsWith("data")) {
                    try {
                        Gson gson = new Gson();
                        CompletePacket data = gson.fromJson(ReadFile(f), CompletePacket.class);
                        SendGpsData(data, context, new RetrofitResponseListener() {
                            final File ff = f;
                            @Override
                            public void onSuccess() {
                                ff.delete();
                            }

                            @Override
                            public void onFailure() {

                            }
                        });



                    } catch (Exception e) {
                        Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    public static String ReadFile(File f) {
        String s = "";
        String UTF8Str = "";
        if (f.exists()) {
            try {
                BufferedReader in = new BufferedReader(new FileReader(f));
                while ((s = in.readLine()) != null) {
                    UTF8Str = UTF8Str + "\n" + new String(s.getBytes(), "UTF-8");
                }
            } catch (IOException e) {
                Log.d("ReadLog", e.toString());
            }
        } else {
            UTF8Str = "";
        }
        return UTF8Str;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Application.CONNECTIVITY_SERVICE);
        NetworkInfo net = connManager.getActiveNetworkInfo();
        if (net != null && net.isConnected() && (net.getType() == ConnectivityManager.TYPE_WIFI || net.getType() == ConnectivityManager.TYPE_MOBILE)) {
            return true;
        } else {
            return false;
        }
    }

    public static void AllEnd(Context context, CompletePacket gpsdataSend) {
        //write json into local internal file
        try {
            Gson gson = new Gson();
            String name = "data" + MyUtiles.IdCreator(6) + "";
            FileOutputStream fos = context.openFileOutput(name + ".json", Context.MODE_PRIVATE);
            fos.write(gson.toJson(gpsdataSend).getBytes("UTF-8"));
            fos.close();
            Toast.makeText(context, "با تشکر از همکاری شما", Toast.LENGTH_SHORT).show();
            ListFilesAndPost(context);
        } catch (IOException e) {

        }
    }

    public static void createNotif(Context context, String msg, String namebtn) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel;
            CharSequence name = "RRM";
            String description = "background noif";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel("Origin", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setAction(Intent.ACTION_MAIN);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        Intent intentaction = new Intent(context, NotifBroadcastReciver.class);
        intentaction.setAction("intentaction");
        PendingIntent btnaction = PendingIntent.getBroadcast(context, 0, intentaction, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "Origin")
                .setSmallIcon(R.drawable.ic_rselogo)
                .setContentTitle("RRM")
                .setSound(null)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentText(msg)
                .setLargeIcon(null)//adres for bank logo
                .setContentIntent(pendingIntent)// Set the intent that will fire when the user taps the notification
                .setAutoCancel(true)
                .setOngoing(true)
                .addAction(R.drawable.ic_rselogo, namebtn, btnaction)
                .setPriority(NotificationCompat.PRIORITY_MAX);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(123, builder.build());// notificationId is a unique int for each notification that you must define

    }

    public static void SendGpsData(CompletePacket data, Context c,final RetrofitResponseListener retrofitResponseListener) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<ResponseBody> call = api.PostGpsData(data, "Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.code()==200 || response.code()==400) { //400 for data has zero speed
                        Log.d("apisendgpsdata", response.code() + "");
                        retrofitResponseListener.onSuccess();
                    }else{
                        Log.d("apisendgpsdata", response.code() + "");
                        retrofitResponseListener.onFailure();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    retrofitResponseListener.onFailure();
                }
            });

        } else {
            retrofitResponseListener.onFailure();
        }
    }

    public static boolean SendSignUp(User data, final Context context, final Button create, final ProgressBar pb, final FragmentActivity currentactivity) {
        if (MyUtiles.isNetworkConnected(context)) {
            final boolean[] res = {false};
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<ResponseBody> call = api.PostSignUp(data);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d("apisendsignup", response.code() + "");
                    if (response.code() == 200) {
                        res[0] = true;
                        Toast.makeText(context, "ثبت نام موفق", Toast.LENGTH_SHORT).show();
                        currentactivity.startActivity(new Intent(context, LoginActivity.class));
                        currentactivity.finish();
                    } else {
                        res[0] = false;
                        Toast.makeText(context, "خطایی رخ داده است", Toast.LENGTH_SHORT).show();
                    }

                    //get back to login

                    pb.setVisibility(View.INVISIBLE);
                    create.setActivated(true);

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    res[0] = false;

                    pb.setVisibility(View.INVISIBLE);
                    create.setActivated(true);

                }
            });
            return res[0];
        } else {
            Toast.makeText(context, "دسترسی به اینترنت وجود ندارد", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static void SignOut(Context context) {
        CarDB carDB = new CarDB(context);
        TakeDB takeDB = new TakeDB(context);
        carDB.delete();
        takeDB.delete();
        CollectData.GetInstance().Stop(0);
        GpsService.GetInstance().Stop();
        GetUserCarsList.getCars().clear();
        GetUserCarsList.getNameAndId().clear();
        MainActivity.mainActivity.getApplication().stopService(MainActivity.mainActivity.intent);
        //-------------------------------delete json from recent session
        File root = new File(context.getFilesDir().getAbsolutePath());
        File files[] = root.listFiles();
        for ( File f : files) {
            if (f.getName().endsWith(".json") && f.getName().startsWith("data")) {
                try {
                    f.delete();
                } catch (Exception e) {

                }
            }
        }

        //stop gpsservice and goto signup signin
        InitActivity.setUsername("null");
        InitActivity.setPassword("null");
        InitActivity.setUserType("null");
        InitActivity.setUserJob("null");
        InitActivity.setUserFamilyName("null");
        InitActivity.setUserFirstName("null");
        InitActivity.setUserPhone("null");
        InitActivity.setUserEmail("null");
        InitActivity.setFreq(100);
        InitActivity.setCarId(0);
        InitActivity.setCurentspeed(0f);
        InitActivity.setCurentmaxspeed(0f);
        InitActivity.setCurentavgspeed(0f);
        InitActivity.setTotallength(0f);
        InitActivity.setValidtotallength(0f);
        InitActivity.setFreqspeed("خالی");
        InitActivity.setMaxspeed(0f);
        InitActivity.setAvgspeed(0f);
        InitActivity.setCurenttotallength(0f);
        InitActivity.setCurentvalidtotallength(0f);
        InitActivity.setStudent_user_id_id( 0);
        InitActivity.setStudent_university( "null");
        InitActivity.setStudent_level("null");
        InitActivity.setStudent_supervisor("null");
        InitActivity.setStudent_topic( "null");
        InitActivity.setStudent_target("null");
        InitActivity.setProfessor_user_id_id( 0);
        InitActivity.setProfessor_university( "null");
        InitActivity.setProfessor_degree("null");
        InitActivity.setProfessor_topic( "null");
        InitActivity.setProfessor_target("null");
        InitActivity.setProject_owner_user_id_id( 0);
        InitActivity.setProject_name("null");
        InitActivity.setProject_start_date( "");
        InitActivity.setProject_organizations_id( 0);
        InitActivity.setProject_user_role( "null");
        InitActivity.setProject_km( 0);
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
        MainActivity.mainActivity.finish();
    }

    public static void GetCars(final Context c,final RetrofitResponseListener retrofitResponseListener) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<GetUserCarsListForFuckEnd> call = api.GetUserCars("Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<GetUserCarsListForFuckEnd>() {
                @Override
                public void onResponse(Call<GetUserCarsListForFuckEnd> call, Response<GetUserCarsListForFuckEnd> response) {
                    Log.d("apigetcar", response.code() + "");
                    Log.d("apigetcar", response.body().toString());
                    if (response.code() == 200) {
                        GetUserCarsList.setCars(response.body().getCars(),c);
                        ReadCarDb(c);
                        Log.d("apigetcar", response.body().toString());
                        retrofitResponseListener.onSuccess();
                    } else {
                           retrofitResponseListener.onFailure();
                    }
                }

                @Override
                public void onFailure(Call<GetUserCarsListForFuckEnd> call, Throwable t) {
                    retrofitResponseListener.onFailure();
                }
            });
        }else{
            retrofitResponseListener.onFailure();
        }

    }

    public static void GetOrgan(Context c, final ArrayAdapter names_Adapter, final ArrayList<String> institutions_names, final AutoCompleteTextView proj_name, final AutoCompleteTextView user_role, final AutoCompleteTextView insss_name) {
        if (MyUtiles.isNetworkConnected(c)) {
            try {
                ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
                Call<JsonObject> call = api.GetOrgan();
                call.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.d("apigetorgan", response.code() + "");
                        if (response.code() == 200) {
                            JSONObject itis = null;
                            try {
                                itis = new JSONObject(response.body().toString());

                                JSONArray itis2 = itis.getJSONArray("data");

                                Log.d("AAAAAAAAAAAAAAA3", response.body().toString());
                                Log.d("AAAAAAAAAAAAAAA3", itis2.toString());

                                for (int i = 0; i < itis2.length(); i++) {
                                    JSONObject j = itis2.getJSONObject(i);
                                    JSONArray a2 = j.getJSONArray("organization_project");
                                    ArrayList<String> organprojs = new ArrayList<>();
                                    organprojs.add(j.getInt("organization_id") + ""); //0 element is id
                                    Log.d("AAAAAAAAAAAAAAA3add", j.getString("organization_id") + "");
                                    for (int ii = 0; ii < a2.length(); ii++) {
                                        organprojs.add(a2.getString(ii));
                                        Log.d("AAAAAAAAAAAAAAA3add", a2.getString(ii));
                                    }
                                    GetOrganList.getOrgans().put(j.getString("organization_name"), organprojs);
                                    Log.d("SSSSSSSSSS","added");

                                }



                                //add it to ed
                                proj_name.setEnabled(true);
                                user_role.setEnabled(true);
                                insss_name.setEnabled(true);


                                for (Object name:GetOrganList.getOrgans().keySet().toArray()) {
                                    institutions_names.add(name.toString());
                                    Log.d("SSSSSSSSSS", name.toString());

                                }
                                names_Adapter.notifyDataSetChanged();




                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {

                        }


                    }


                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(c, "دسترسی به اینترنت وجود ندارد", Toast.LENGTH_SHORT).show();
        }

    }

    public static boolean GetLogin(String username, String password, final Context context, final FragmentActivity activity, final TextInputEditText pass, final FragmentActivity CurrentACtivity, final ProgressBar pb) throws JSONException {
        if (MyUtiles.isNetworkConnected(context)) {
            final boolean[] res = {false};
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            JsonObject user = new JsonObject();
            user.addProperty("username", username);
            user.addProperty("password", password);
            Call<LoginResponse> call = api.Login(user);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.d("apigetlogin", response.code() + "");
                    if (response.code() == 200) {
                        res[0] = true;
                        InitActivity.setUserId(response.body().getUserid());
                        InitActivity.setUsername(response.body().getUsername());
                        InitActivity.setTokenRefresh(response.body().getRefresh());
                        InitActivity.setTokenAccess(response.body().getAccess());
                        try {
                            GetUserInfo(context);
                            GetCars(context, new RetrofitResponseListener() {
                                @Override
                                public void onSuccess() {

                                }

                                @Override
                                public void onFailure() {

                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        pb.setVisibility(View.INVISIBLE);
                        Toast.makeText(context, "ورود موفق", Toast.LENGTH_SHORT).show();
                        InitActivity.setPassword(pass.getText().toString());
                        CurrentACtivity.finish();
                        activity.startActivity(new Intent(activity, MainActivity.class));//bug
                    } else {
                        pb.setVisibility(View.INVISIBLE);
                        Toast.makeText(context, "خطایی رخ داده است", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {

                }
            });
            return res[0];
        } else {
            Toast.makeText(context, "دسترسی به اینترنت وجود ندارد", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static void VerifyToken(final Context c) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            JsonObject j = new JsonObject();
            j.addProperty("token", InitActivity.getTokenAccess());
            Call<ResponseBody> call = api.Verify(j);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d("apiverifytoken", response.code() + "");
                    if (response.code() == 401) {
                        try {
                            RefreshToken(c);
                        } catch (JSONException e) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }

    public static void RefreshToken(final Context c) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            JsonObject j = new JsonObject();
            j.addProperty("refresh", InitActivity.getTokenRefresh());
            Call<RefreshResponse> call = api.Refresh(j);
            call.enqueue(new Callback<RefreshResponse>() {
                @Override
                public void onResponse(Call<RefreshResponse> call, Response<RefreshResponse> response) {
                    Log.d("apirefreshtoken", response.code() + "");
                    if (response.code() == 200) {
                        InitActivity.setTokenAccess(response.body().getAccess());
                    } else if (response.code() == 401) {
                        SignOut(c);
                        Toast.makeText(c, "لطفا مجدد وارد شوید", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RefreshResponse> call, Throwable t) {

                }
            });
        }
    }

    public static void UpdateUserInfo(final User user, Context c,final RetrofitResponseListener retrofitResponseListener) throws JSONException {
        Log.d("apiupdateuserinfo", "1");
        if (MyUtiles.isNetworkConnected(c)) {
            Log.d("apiupdateuserinfo", "2");
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<ResponseBody> call = api.UpdateUserInfo(user, "Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d("apiupdateuserinfo", "3");
                    Log.d("apiupdateuserinfo", response.code() + "");
                    if (response.code() == 200) {
                        InitActivity.setUserPhone(user.getPhone());
                        Log.d("apiupdateuserinfo", user.getPhone());
                        InitActivity.setUserEmail(user.getEmail());
                        InitActivity.setUserFirstName(user.getFirstname());
                        InitActivity.setUserFamilyName(user.getLastname());
                        //-----------------
                        InitActivity.setStudent_university(user.getStu_university());
                        InitActivity.setStudent_level(user.getStu_level());
                        InitActivity.setStudent_topic(user.getStu_topic());
                        InitActivity.setStudent_target(user.getStu_target());
                        InitActivity.setStudent_supervisor(user.getStu_supervisor());
                        //----------------------
                        InitActivity.setProfessor_target(user.getPro_target());
                        InitActivity.setProfessor_topic(user.getPro_topic());
                        InitActivity.setProfessor_degree(user.getPro_level());
                        InitActivity.setProfessor_university(user.getPro_university());
                        //----------------------
                        InitActivity.setProject_user_role(user.getProjectuserrole());
                        InitActivity.setProject_name(user.getProjectname());
                        try {
                            InitActivity.setProject_km(Integer.parseInt(user.getProjectkm()));
                        }catch (Exception e){
                            Log.d("apiupdateuserinfo", e.getMessage());
                            InitActivity.setProject_km(0);
                        }

                        retrofitResponseListener.onSuccess();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("apiupdateuserinfo", t.getMessage());
                     retrofitResponseListener.onFailure();
                }
            });

        } else {
            Toast.makeText(c, "دسترسی به اینترنت وجود ندارد", Toast.LENGTH_SHORT).show();
            retrofitResponseListener.onFailure();
        }
    }

    public static void GetUserInfo(Context c) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<UserResponse> call = api.GetUserData("Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<UserResponse>() {
                @Override
                public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                    Log.d("apigetuserinfo", response.code() + "");
                    if (response.code() == 200) {
                        switch (response.body().getUser_type()) {
                            case "regular":
                                InitActivity.setUserId(response.body().getId());
                                InitActivity.setUsername(response.body().getUsername());
                                InitActivity.setUserPhone(response.body().getUser_phone());
                                InitActivity.setUserEmail(response.body().getEmail());
                                InitActivity.setUserType(response.body().getUser_type());
                                InitActivity.setUserFirstName(response.body().getUser_first_name());
                                InitActivity.setUserFamilyName(response.body().getUser_last_name());
                                InitActivity.setUserJob(response.body().getUser_job());
                                break;
                            case "student":
                                InitActivity.setUserId(response.body().getId());
                                InitActivity.setUsername(response.body().getUsername());
                                InitActivity.setUserPhone(response.body().getUser_phone());
                                InitActivity.setUserEmail(response.body().getEmail());
                                InitActivity.setUserType(response.body().getUser_type());
                                InitActivity.setUserFirstName(response.body().getUser_first_name());
                                InitActivity.setUserFamilyName(response.body().getUser_last_name());
                                //--------------
                                InitActivity.setStudent_user_id_id(response.body().getStudent_user_id_id());
                                InitActivity.setStudent_university(response.body().getStudent_university());
                                InitActivity.setStudent_level(response.body().getStudent_level());
                                InitActivity.setStudent_topic(response.body().getStudent_topic());
                                InitActivity.setStudent_target((response.body().getStudent_target()));
                                InitActivity.setStudent_supervisor(response.body().getStudent_supervisor());
                                break;
                            case "professor":
                                InitActivity.setUserId(response.body().getId());
                                InitActivity.setUsername(response.body().getUsername());
                                InitActivity.setUserPhone(response.body().getUser_phone());
                                InitActivity.setUserEmail(response.body().getEmail());
                                InitActivity.setUserType(response.body().getUser_type());
                                InitActivity.setUserFirstName(response.body().getUser_first_name());
                                InitActivity.setUserFamilyName(response.body().getUser_last_name());
                                //--------------
                                InitActivity.setProfessor_user_id_id(response.body().getProfessor_user_id_id());
                                InitActivity.setProfessor_target(response.body().getProfessor_target());
                                InitActivity.setProfessor_topic(response.body().getProfessor_topic());
                                InitActivity.setProfessor_degree(response.body().getProfessor_degree());
                                InitActivity.setProfessor_university(response.body().getProfessor_university());
                                break;
                            case "project":
                                InitActivity.setUserId(response.body().getId());
                                InitActivity.setUsername(response.body().getUsername());
                                InitActivity.setUserPhone(response.body().getUser_phone());
                                InitActivity.setUserEmail(response.body().getEmail());
                                InitActivity.setUserType(response.body().getUser_type());
                                InitActivity.setUserFirstName(response.body().getUser_first_name());
                                InitActivity.setUserFamilyName(response.body().getUser_last_name());
                                //--------------
                                InitActivity.setProject_owner_user_id_id(response.body().getProject_owner_user_id_id());
                                InitActivity.setProject_user_role(response.body().getProject_user_role());
                                InitActivity.setProject_organizations_id(response.body().getProject_organizations_id());
                                InitActivity.setProject_start_date(response.body().getProject_start_date());
                                InitActivity.setProject_name(response.body().getProject_name());
                                InitActivity.setProject_km(response.body().getProject_km());
                                break;
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserResponse> call, Throwable t) {

                }
            });
        }
    }

    public static void ReadCarDb(Context context) {
        CarDB db = new CarDB(context);
        Cursor cursor = db.getReadableDatabase().rawQuery("SELECT * FROM '" + "tb_cars" + "'", null);
        if (cursor.moveToLast()) {
            do {
                Car car = new Car();
                car.setCarid(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Car.KeyId))));
                car.setCarname(cursor.getString(cursor.getColumnIndex(Car.KeyName)));
                car.setCreatyear(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Car.KeyCreation))));
                car.setUsagekm(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Car.KeyUsage))));
                car.setCarlength(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Car.Keylength))));
                car.setSelected(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(Car.KeySelect))));
                GetUserCarsList.getCars().add(car);
                GetUserCarsList.getNameAndId().put(Integer.parseInt(cursor.getString(cursor.getColumnIndex(Car.KeyId))), cursor.getString(cursor.getColumnIndex(Car.KeyName)));
            } while (cursor.moveToPrevious());
        }
        cursor.close();
    }

    public static void ReadTakeDb(Context context) {
        GetTakeList.getTakes().clear();
        TakeDB db = new TakeDB(context);
        Cursor cursor = db.getReadableDatabase().rawQuery("SELECT * FROM '" + "tb_takes" + "'", null);
        if (cursor.moveToLast()) {
            do {
                GetTakeResponse take = new GetTakeResponse();
                take.setCarname(cursor.getString(cursor.getColumnIndex(TakeDB.KeyModel)));
                take.setDesc(cursor.getString(cursor.getColumnIndex(TakeDB.KeyDesc)));
                take.setStart(cursor.getString(cursor.getColumnIndex(TakeDB.KeyDate)));
                take.setTotallen(Integer.parseInt(cursor.getString(cursor.getColumnIndex(TakeDB.KeyLength))));
                GetTakeList.getTakes().add(take);
            } while (cursor.moveToPrevious());
        }
        cursor.close();
    }

    public static void GetSpeeds(final String param, Context c) throws JSONException { //can be max or avg
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<JsonObject> call = api.GetSpeed(param, "Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("apigetspeed", response.code() + "");
                    if (response.code() == 200) {
                        try {
                            JSONObject itis = new JSONObject(response.body().toString());
                            switch (param) {
                                case "max":
                                    InitActivity.setMaxspeed(itis.getInt("survay_mean_velocity__max"));
                                    break;
                                case "avg":
                                    InitActivity.setAvgspeed(itis.getInt("survay_mean_velocity__avg"));
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }
    }

    public static void GetFreqSpeed(Context c) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<JsonObject> call = api.GetFreqSpeed("Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("apigetfreqspeed", response.code() + "");
                    if (response.code() == 200) {
                        try {
                            JSONObject itis = new JSONObject(response.body().toString());
                            InitActivity.setFreqspeed(itis.getString("survay_most_frequent_velocity_range"));
                        } catch (JSONException e) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }
    }

    public static void GetTotalDis(final String param, Context c) throws JSONException { //can be 0 or 1 for all and valid
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<JsonObject> call = api.GetTotalDis(param, "Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("apigettotaldis", response.code() + "" + response.body().toString());
                    if (response.code() == 200) {
                        try {
                            JSONObject itis = new JSONObject(response.body().toString());
                            switch (param) {
                                case "0":
                                    InitActivity.setTotallength(itis.getInt("survay_total_len__sum"));
                                    break;
                                    case "1":
                                    InitActivity.setValidtotallength(itis.getInt("survay_valid_total_len__sum"));
                                    break;
                            }
                        } catch (JSONException e) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }
    }

    public static void GetTotalDisPerCar(final Context c) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<JsonObject> call = api.GetTotalDisPerCar("Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d("apigettotaldispercar", response.code() + "");
                    if (response.code() == 200) {
                        try {
                            JSONObject itis = new JSONObject(response.body().toString());
                            CarDB db = new CarDB(c);
                            JSONArray array = itis.getJSONArray("data");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject a = array.getJSONObject(i);
                                int carid = a.getInt("survay_car_id");
                                int carlen = a.getInt("total_len_car")/1000; //to show in km
                                for (Car car : GetUserCarsList.getCars()) {
                                    if (car.getCarid() == carid) {
                                        car.setCarlength(carlen);
                                        db.UpdateDB(car);
                                    }
                                }
                            }
                        } catch (Exception e) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {

                }
            });
        }
    }

    public static void GetTakes(Context c ,final RetrofitResponseListener retrofitResponseListener) throws JSONException {
        Log.d("apisinaaaa", "1");
        if (MyUtiles.isNetworkConnected(c)) {
            Log.d("apisinaaaa", "2");
            VerifyToken(c);
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<GetTakeListForFuck> call = api.GetSurvayData("Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<GetTakeListForFuck>() {
                @Override
                public void onResponse(Call<GetTakeListForFuck> call, Response<GetTakeListForFuck> response) {
//                    Log.d("apisinaaaa", response.code()+" "+response.body().getTakes().size());
                    if (response.code() == 200) {
                        if(response.body().getTakes()!=null) {
                            GetTakeList.setTakes(response.body().getTakes());
                        }
                        retrofitResponseListener.onSuccess();
                    }
                }

                @Override
                public void onFailure(Call<GetTakeListForFuck> call, Throwable t) {
                    Log.d("apisinaaaa", t.getMessage());
                   retrofitResponseListener.onFailure();
                }
            });
        }
    }

    public static void AddCar(Car car, final Context c ,final RetrofitResponseListener retrofitResponseListener) throws JSONException {
        if (MyUtiles.isNetworkConnected(c)) {
            VerifyToken(c);
           // final boolean trueone=true;
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<ResponseBody> call = api.PostCarAdd(car, "Bearer " + InitActivity.getTokenAccess());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.d("apiaddcar", response.code() + "");
                    if (response.code() == 200) {
                        retrofitResponseListener.onSuccess();
                        Toast.makeText(c, "خودرو اضافه شد", Toast.LENGTH_SHORT).show();
                    } else {
                        retrofitResponseListener.onFailure();
                        Toast.makeText(c, "خظایی رخ داده است", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    retrofitResponseListener.onFailure();
                }

            });
        } else {
            Toast.makeText(c, "دسترسی به اینترنت وجود ندارد", Toast.LENGTH_SHORT).show();
            retrofitResponseListener.onFailure();
        }
    }

    public static void GetCarModels(Context c, final ArrayAdapter arrayAdapter, final ArrayList<String> brands, final AutoCompleteTextView brand) {
        if (MyUtiles.isNetworkConnected(c)) {
            ApiFunctions api = ApiClient.getClient().create(ApiFunctions.class);
            Call<JsonObject> call = api.GetCarModels();
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {
                    Log.d("apigetcarmodel", response.code() + "");
                    if (response.code() == 200) {
                        JSONObject itis = null;
                        try {
                            itis = new JSONObject(response.body().toString());
                            JSONObject itis2 = itis.getJSONObject("data");

                            Log.d("AAAAAAAAAAAAAAA", response.body().toString());
                            Log.d("AAAAAAAAAAAAAAA2", itis2.toString());

                            Iterator<?> keys = itis2.keys();
                            while (keys.hasNext()) {
                                String key = (String) keys.next();
                                Log.d("AAAAAAAAAAAAAAA2", key);
                                if (itis2.get(key) instanceof JSONArray) {// check if is jsonarray like saipa,ik...
                                    JSONArray curent = itis2.getJSONArray(key);
                                    ArrayList<CarModels> carModels = new ArrayList<>();
                                    for (int i = 0; i < curent.length(); i++) {
                                        JSONObject j = curent.getJSONObject(i);
                                        CarModels cc = new CarModels(j.getString("car_model"), j.getInt("car_model_id"));
                                        carModels.add(cc);
                                        Log.d("AAAAAAAAAAAAAAA2", j.getString("car_model") + "---" + j.getInt("car_model_id"));
                                        Log.d("AAAAAAAAAAAAAAA", cc.getModel());

                                    }
                                    GetServerCarsModel.getCars().put(key, carModels);
                                }
                            }


                            brand.setEnabled(true);
                            final HashMap<String, ArrayList<CarModels>> map = GetServerCarsModel.getCars();
                            for (Object b : map.keySet().toArray()) {
                                brands.add(b.toString());

                            }
                            arrayAdapter.notifyDataSetChanged();
                            //

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    Log.d("apigetcarmodel", t.getMessage());
                }
            });
        }else{
            Toast.makeText(c, "دسترسی به اینترنت وجود ندارد", Toast.LENGTH_SHORT).show();
        }
    }
}
