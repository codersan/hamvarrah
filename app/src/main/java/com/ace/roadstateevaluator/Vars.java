package com.ace.roadstateevaluator;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

public class Vars {

    public static int width =-1,height =-1;

    public static void calculate_size(WindowManager windowManager){
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display. getSize(size);
        height = size. y;
        width = size. x;
    }

}
