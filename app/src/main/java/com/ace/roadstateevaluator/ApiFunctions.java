package com.ace.roadstateevaluator;

import com.ace.roadstateevaluator.GsonModels.Car;
import com.ace.roadstateevaluator.GsonModels.CompletePacket;
import com.ace.roadstateevaluator.GsonModels.GetServerCarsModel;
import com.ace.roadstateevaluator.GsonModels.GetTakeListForFuck;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.GsonModels.GetTakeList;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsListForFuckEnd;
import com.ace.roadstateevaluator.GsonModels.LoginResponse;
import com.ace.roadstateevaluator.GsonModels.RefreshResponse;
import com.ace.roadstateevaluator.GsonModels.User;
import com.ace.roadstateevaluator.GsonModels.UserResponse;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiFunctions {


    @POST("/rrm/send-data/")
    Call<ResponseBody> PostGpsData(@Body CompletePacket data,@Header("Authorization") String authHeader);

    @POST("/rrm/add-user/")
    Call<ResponseBody> PostSignUp(@Body User data);

    @POST("/rrm/save-car/")
    Call<ResponseBody> PostCarAdd(@Body Car car,@Header("Authorization") String authHeader);

    @POST("/rrm/token/")
    Call<LoginResponse> Login(@Body JsonObject object);

    @POST("/rrm/token/verify/")
    Call<ResponseBody> Verify(@Body JsonObject object);

    @POST("/rrm/token/refresh/")
    Call<RefreshResponse> Refresh(@Body JsonObject object);

    @POST("/rrm/edit-user-data/")
    Call<ResponseBody> UpdateUserInfo(@Body User user,@Header("Authorization") String authHeader);

    @GET("/rrm/get-user-cars")
    Call<GetUserCarsListForFuckEnd> GetUserCars(@Header("Authorization") String authHeader);

    @GET("/rrm/get-car-models")
    Call<JsonObject> GetCarModels();

    @GET("/rrm/get-organization-projects/")
    Call<JsonObject> GetOrgan();

    @GET("/rrm/get-user-speed-statistics/{param}/") //avg or max
    Call<JsonObject> GetSpeed(@Path("param") String parametr,@Header("Authorization") String authHeader);

    @GET("/rrm/get-user-frequent-speed")
    Call<JsonObject> GetFreqSpeed(@Header("Authorization") String authHeader);

    @GET("/rrm/get-user-total-distance-per-car")
    Call<JsonObject> GetTotalDisPerCar(@Header("Authorization") String authHeader);

    @GET("/rrm/get-user-total-distance/{param}/") //0 for all , 1 for valid
    Call<JsonObject> GetTotalDis( @Path("param") String parametr,@Header("Authorization") String authHeader);

    @GET("/rrm/get-user-survays")
    Call<GetTakeListForFuck> GetSurvayData(@Header("Authorization") String authHeader);

    @GET("/rrm/get-user-data")
    Call<UserResponse> GetUserData(@Header("Authorization") String authHeader);

}
