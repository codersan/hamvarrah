package com.ace.roadstateevaluator;

public interface RetrofitResponseListener {
    void onSuccess();

    void onFailure();
}
