package com.ace.roadstateevaluator;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;

import static androidx.core.app.ActivityCompat.requestPermissions;

public class GpsService extends Service {
    private static GpsService gpsService;
    private static LocationManager locationmanager;
    private static LocationListener listener;
    private NotificationManagerCompat notificationManager;
    static {
        if(gpsService==null)gpsService= new GpsService();
    }

    public GpsService() {

    }
    public static GpsService GetInstance(){
        return gpsService;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.d("sinaaaaaaaaa", "back-onCreate: ");
        startForeground(123, createNotif("توقف برداشت داده!!!", "شروع داده برداری"));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // do your jobs here
        Log.d("sinaaaaaaaaa", "created");
        return super.onStartCommand(intent, flags, startId);
    }

    public static void remove() { //this use when stop btn clicked and stop geting location
        if(locationmanager!=null&&listener!=null) {
            locationmanager.removeUpdates(listener);
        }
    }

    public void StartCapture() {
        try {


            Log.d("sinaaaaaaaaa", "start capture");
            locationmanager = (LocationManager) MainActivity.mainActivity.getSystemService(LOCATION_SERVICE);
            listener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    CollectData.GetInstance().LocationGetting(location);
                    //Log.d("sinaaaaaa", location.getLatitude() + "---" + location.getLongitude());
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                   // Toast.makeText(getApplicationContext(), "provider enabled", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onProviderDisabled(String s) {
                   // Toast.makeText(getApplicationContext(), "provider disabled", Toast.LENGTH_SHORT).show();
                }
            };

            Log.d("sinaaaaaa", "checkupdate");

            if (ActivityCompat.checkSelfPermission(MainActivity.mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.mainActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationmanager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 15, listener);
            Log.d("sinaaaaaa", "update");
        }catch (Exception e){

        }

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d("sinaaaaaaaaa","destroy");
        InitActivity.setCurentspeed(0f);
        InitActivity.setCurentvalidtotallength(0f);
        InitActivity.setCurenttotallength(0f);
        //InitActivity.setCurentavgspeed(0f);
        //InitActivity.setCurentmaxspeed(0f);
        super.onTaskRemoved(rootIntent);
        stopForeground(true);
        Stop();
    }
    public void Stop(){ // this is used for all stop service for exit app or sign out , it starts from mainactivity
        Log.d("sinaaaaaaaaa","stop gps");
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(MainActivity.mainActivity);
        notificationManager.cancelAll();
        // Destroy the service
        stopSelf();
    }

    private Notification createNotif(String msg, String namebtn) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel;
            CharSequence name = "RRM";
            String description = "background notification";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel("Origin", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addCategory(Intent. CATEGORY_LAUNCHER ) ;
        intent.setAction(Intent. ACTION_MAIN ) ;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Intent intentaction = new Intent(this, NotifBroadcastReciver.class);
        intentaction.setAction("intentaction");
        PendingIntent btnaction = PendingIntent.getBroadcast(this, 0, intentaction, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "Origin")
                .setSmallIcon(R.drawable.ic_rselogo)
                .setContentTitle("RRM")
                .setSound(null)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentText(msg)
                .setLargeIcon(null)//adres for bank logo
                .setContentIntent(pendingIntent)// Set the intent that will fire when the user taps the notification
                .setAutoCancel(true)
                .setOngoing(true)
                .addAction(R.drawable.ic_rselogo, namebtn, btnaction)
                .setPriority(NotificationCompat.PRIORITY_MAX);

        notificationManager = NotificationManagerCompat.from(this);

        return builder.build();
    }

}
