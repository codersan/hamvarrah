package com.ace.roadstateevaluator.GsonModels;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

public class DataPacket {
    @SerializedName("his_date_time")
    private String datetime="d";
    @SerializedName("l_one_x")
    private String l1x="d";
    @SerializedName("l_one_y")
    private String l1y="d";
    @SerializedName("l_one_acc")
    private String l1acc="d";
    @SerializedName("l_two_x")
    private String l2x="d";
    @SerializedName("l_two_y")
    private String l2y="d";
    @SerializedName("l_two_acc")
    private String l2acc="d";
    @SerializedName("length")
    private float length=1;
    @SerializedName("average_velocity")
    private float averagevelocity=1;
    @SerializedName("rms")
    private float rms=1;
    //--------------------------------
    public DataPacket() {
    }

    public DataPacket(String datetime, String l1x, String l1y, String l1acc, String l2x, String l2y, String l2acc, float length, float averagevelocity, float rms) {
        this.datetime = datetime;
        this.l1x = l1x;
        this.l1y = l1y;
        this.l1acc = l1acc;
        this.l2x = l2x;
        this.l2y = l2y;
        this.l2acc = l2acc;
        this.length = length;
        this.averagevelocity = averagevelocity;
        this.rms = rms;
    }
    public void SetLocation1(Location location){
        if (location != null) {
            setL1x(location.getLatitude()+"");
            setL1y(location.getLongitude()+"");
            setL1acc(location.getAccuracy()+"");
        }else{
            setL1x("");
            setL1y("");
            setL1acc("");
        }

    }
    public void SetLocation2(Location location){
        if (location != null) {
            setL2x(location.getLatitude()+"");
            setL2y(location.getLongitude()+"");
            setL2acc(location.getAccuracy()+"");
        }else{
            setL2x("");
            setL2y("");
            setL2acc("");
        }
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setL1x(String l1x) {
        this.l1x = l1x;
    }

    public void setL1y(String l1y) {
        this.l1y = l1y;
    }

    public void setL1acc(String l1acc) {
        this.l1acc = l1acc;
    }

    public void setL2x(String l2x) {
        this.l2x = l2x;
    }

    public void setL2y(String l2y) {
        this.l2y = l2y;
    }

    public void setL2acc(String l2acc) {
        this.l2acc = l2acc;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public void setAveragevelocity(float averagevelocity) {
        this.averagevelocity = averagevelocity;
    }

    public void setRms(float rms) {
        this.rms = rms;
    }
    public String getDatetime() {
        return datetime;
    }

    public String getL1x() {
        return l1x;
    }

    public String getL1y() {
        return l1y;
    }

    public String getL1acc() {
        return l1acc;
    }

    public String getL2x() {
        return l2x;
    }
    public String getL2y() {
        return l2y;
    }

    public String getL2acc() {
        return l2acc;
    }

    public float getLength() {
        return length;
    }

    public float getAveragevelocity() {
        return averagevelocity;
    }

    public float getRms() {
        return rms;
    }
}
