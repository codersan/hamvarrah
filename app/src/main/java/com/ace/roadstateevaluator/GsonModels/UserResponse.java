package com.ace.roadstateevaluator.GsonModels;

public class UserResponse {
    private int id;
    private String username;
    private String user_first_name;
    private String user_last_name;
    private String user_phone;
    private String user_type;
    private String user_job;
    private String email;
    private boolean is_staff;
    private boolean is_active;
    //---------------------------
    private int student_user_id_id;
    private String student_university;
    private String student_level;
    private String student_supervisor;
    private String student_topic;
    private String student_target;
    //------------------------------
    private int professor_user_id_id;
    private String professor_university;
    private String professor_degree;
    private String professor_topic;
    private String professor_target;
    //--------------------------------
    private int project_owner_user_id_id;
    private String project_name;
    private String project_start_date;
    private int project_organizations_id;
    private String project_user_role;
    private int project_km;
    //-------------------------------


    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public void setUser_job(String user_job) {
        this.user_job = user_job;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIs_staff(boolean is_staff) {
        this.is_staff = is_staff;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public void setStudent_user_id_id(int student_user_id_id) {
        this.student_user_id_id = student_user_id_id;
    }

    public void setStudent_university(String student_university) {
        this.student_university = student_university;
    }

    public void setStudent_level(String student_level) {
        this.student_level = student_level;
    }

    public void setStudent_supervisor(String student_supervisor) {
        this.student_supervisor = student_supervisor;
    }

    public void setStudent_topic(String student_topic) {
        this.student_topic = student_topic;
    }

    public void setStudent_target(String student_target) {
        this.student_target = student_target;
    }

    public void setProfessor_user_id_id(int professor_user_id_id) {
        this.professor_user_id_id = professor_user_id_id;
    }

    public void setProfessor_university(String professor_university) {
        this.professor_university = professor_university;
    }

    public void setProfessor_degree(String professor_degree) {
        this.professor_degree = professor_degree;
    }

    public void setProfessor_topic(String professor_topic) {
        this.professor_topic = professor_topic;
    }

    public void setProfessor_target(String professor_target) {
        this.professor_target = professor_target;
    }

    public void setProject_owner_user_id_id(int project_owner_user_id_id) {
        this.project_owner_user_id_id = project_owner_user_id_id;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public void setProject_start_date(String project_start_date) {
        this.project_start_date = project_start_date;
    }

    public void setProject_organizations_id(int project_organizations_id) {
        this.project_organizations_id = project_organizations_id;
    }

    public void setProject_user_role(String project_user_role) {
        this.project_user_role = project_user_role;
    }

    public void setProject_km(int project_km) {
        this.project_km = project_km;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public String getUser_type() {
        return user_type;
    }

    public String getUser_job() {
        return user_job;
    }

    public String getEmail() {
        return email;
    }

    public boolean isIs_staff() {
        return is_staff;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public int getStudent_user_id_id() {
        return student_user_id_id;
    }

    public String getStudent_university() {
        return student_university;
    }

    public String getStudent_level() {
        return student_level;
    }

    public String getStudent_supervisor() {
        return student_supervisor;
    }

    public String getStudent_topic() {
        return student_topic;
    }

    public String getStudent_target() {
        return student_target;
    }

    public int getProfessor_user_id_id() {
        return professor_user_id_id;
    }

    public String getProfessor_university() {
        return professor_university;
    }

    public String getProfessor_degree() {
        return professor_degree;
    }

    public String getProfessor_topic() {
        return professor_topic;
    }

    public String getProfessor_target() {
        return professor_target;
    }

    public int getProject_owner_user_id_id() {
        return project_owner_user_id_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public String getProject_start_date() {
        return project_start_date;
    }

    public int getProject_organizations_id() {
        return project_organizations_id;
    }

    public String getProject_user_role() {
        return project_user_role;
    }

    public int getProject_km() {
        return project_km;
    }
}
