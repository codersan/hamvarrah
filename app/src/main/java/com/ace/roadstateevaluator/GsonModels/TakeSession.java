package com.ace.roadstateevaluator.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TakeSession {
    @SerializedName("survay_user_id")
    private int userid=0;
    @SerializedName("survay_car_id")
    private int carid=0;
    @SerializedName("survay_mean_velocity")
    private float meanvelocity=0;
    @SerializedName("survay_max_velocity")
    private float maxvelocity=0;
    @SerializedName("survay_total_len")
    private float totallen=0 ;
    @SerializedName("survay_start")
    private String starttime="";
    @SerializedName("survay_desc")
    private String description="";
    //-----------------------------------------
    public TakeSession() {
    }
    public TakeSession(int userid, int carid, int meanvelocity, int maxvelocity, int totallen, String starttime) {
        this.userid = userid;
        this.carid = carid;
        this.meanvelocity = meanvelocity;
        this.maxvelocity = maxvelocity;
        this.totallen = totallen;
        this.starttime = starttime;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public void setMeanvelocity(float meanvelocity) {
        this.meanvelocity = meanvelocity;
    }

    public void setMaxvelocity(float maxvelocity) {
        this.maxvelocity = maxvelocity;
    }

    public void setTotallen(float totallen) {
        this.totallen = totallen;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public int getCarid() {
        return carid;
    }

    public float getMeanvelocity() {
        return meanvelocity;
    }

    public float getMaxvelocity() {
        return maxvelocity;
    }

    public float getTotallen() {
        return totallen;
    }

    public String getStarttime() {
        return starttime;
    }
}
