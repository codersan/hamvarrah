package com.ace.roadstateevaluator.GsonModels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

//order doesnt matter in json and json is an unordered paird of key values!
public class CompletePacket {
    @SerializedName("info")
    private TakeSession info;
    @SerializedName("survay_data")
    private ArrayList<DataPacket> gpsdata=new ArrayList<>();
    //---------------------------------
    public CompletePacket() {
    }
    public CompletePacket(TakeSession info, ArrayList<DataPacket> gpsdata) {
        this.info = info;
        this.gpsdata = gpsdata;
    }

    public ArrayList<DataPacket> getGpsdata() {
        return gpsdata;
    }
    public void setGpsdata(ArrayList<DataPacket> gpsdata) {
        this.gpsdata = gpsdata;
    }
    public TakeSession getInfo() {
        return info;
    }
    public void setInfo(TakeSession info) {
        this.info = info;
    }
}
