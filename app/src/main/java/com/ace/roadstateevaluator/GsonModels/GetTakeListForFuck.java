package com.ace.roadstateevaluator.GsonModels;

import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.DB.TakeDB;
import com.ace.roadstateevaluator.MyUtiles;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetTakeListForFuck {
    //---------------------------
    @SerializedName("data")
    private  ArrayList<GetTakeResponse> takes;

    //-------------------------
    public GetTakeListForFuck() {

    }

    public  ArrayList<GetTakeResponse> getTakes() {
        return takes;
    }
    public  void setTakes(ArrayList<GetTakeResponse> takes) {
        this.takes = takes;

    }
}
