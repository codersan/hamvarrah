package com.ace.roadstateevaluator.GsonModels;

import java.util.ArrayList;
import java.util.HashMap;

public class GetOrganList {
    private static HashMap<String, ArrayList<String>> Organ; //first string is id key for organ like 0 or 1

    static {
        if(Organ==null){Organ=new HashMap<>();}
    }
    //-------------------------
    public GetOrganList() {

    }

    public static HashMap<String, ArrayList<String>> getOrgans() {
        return Organ;
    }

    public static void setOrgans(HashMap<String, ArrayList<String>> organ) {
        GetOrganList.Organ = organ;
    }
}
