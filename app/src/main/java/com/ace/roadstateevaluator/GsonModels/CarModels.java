package com.ace.roadstateevaluator.GsonModels;

import com.google.gson.annotations.SerializedName;

public class CarModels {
    private String model="";
    private int id=0;


    public CarModels(String model, int id) {
        this.model = model;
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public int getId() {
        return id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setId(int id) {
        this.id = id;
    }
}
