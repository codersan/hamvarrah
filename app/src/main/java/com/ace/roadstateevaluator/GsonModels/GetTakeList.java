package com.ace.roadstateevaluator.GsonModels;

import android.util.Log;

import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.DB.CarDB;
import com.ace.roadstateevaluator.DB.TakeDB;
import com.ace.roadstateevaluator.MyUtiles;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class GetTakeList {
    //---------------------------
    @SerializedName("data")
    private static ArrayList<GetTakeResponse> takes=null;
    static {
        if(takes==null){takes=new ArrayList<>();}
    }
    //-------------------------
    public GetTakeList() {

    }

    public static ArrayList<GetTakeResponse> getTakes() {
        return takes;
    }
    public static void setTakes(ArrayList<GetTakeResponse> takes) {
        Collections.reverse(takes);
        GetTakeList.takes.clear();
        for (GetTakeResponse r:takes) {
            GetTakeList.takes.add(r);
        }
        //Collections.reverse( GetTakeList.takes);
        //GetTakeList.takes = takes; change the adress of refrence for notify datachange and adapter
        TakeDB db = new TakeDB(MainActivity.mainActivity);
        db.delete();
        for (GetTakeResponse take : takes){
            db.Insert(take);
        }
        //MyUtiles.ReadTakeDb(MainActivity.mainActivity);
        Log.d("apisinaaa", takes.get(0)+"----   "+takes.size());
    }
}
