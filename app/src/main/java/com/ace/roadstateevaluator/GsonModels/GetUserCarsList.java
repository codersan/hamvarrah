package com.ace.roadstateevaluator.GsonModels;

import android.content.Context;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.DB.CarDB;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class GetUserCarsList {
    //---------------------------
    @SerializedName("cars")
    private static ArrayList<Car> cars=null;
    @Expose(serialize = false, deserialize = false)
    private static HashMap<Integer, String> NameAndId;
    static {
        if(cars==null){cars=new ArrayList<>();}
        if(NameAndId==null){NameAndId=new HashMap<>();}
    }
    //-------------------------
    public GetUserCarsList() {
    }

    public static ArrayList<Car> getCars() {
        return cars;
    }
    public static void setCars(ArrayList<Car> cars, Context c) {
        //GetUserCarsList.cars = cars;
        CarDB db = new CarDB(c);
        db.delete();
        GetUserCarsList.cars.clear();
        GetUserCarsList.NameAndId.clear();
        for (Car car : cars){
            db.Insert(car);
        }
        InitActivity.setCarId(0);
    }

    public static HashMap<Integer, String> getNameAndId() {
        if(!NameAndId.containsKey(0)){
            NameAndId.put(0,"null");
        }
        return NameAndId;
    }

    public static void setNameAndId(HashMap<Integer, String> nameAndId) {
        NameAndId = nameAndId;
    }
    @Override
    public String toString(){
        String a="null";
        for (Car car : cars){
            a+=car.getCarname();
        }
        return a;
    }
}
