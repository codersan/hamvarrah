package com.ace.roadstateevaluator.GsonModels;

public class RefreshResponse {
    private String access;

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }
}
