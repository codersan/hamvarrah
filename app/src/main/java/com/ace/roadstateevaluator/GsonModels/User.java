package com.ace.roadstateevaluator.GsonModels;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.MyUtiles;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("user_first_name")
    private String firstname;
    @SerializedName("user_last_name")
    private String lastname;
    @SerializedName("user_name")
    private String username;
    @SerializedName("user_email")
    private String email;
    @SerializedName("user_phone")
    private String phone;
    @SerializedName("user_password")
    private String password;
    @SerializedName("user_type")
    private String usertype;
    @SerializedName("user_job")
    private String userjob;
    //------------------------------
    //for student details
    @SerializedName("student_university")
    private String stu_university = "";
    @SerializedName("student_level")
    private String stu_level = "";
    @SerializedName("student_supervisor")
    private String stu_supervisor = "";
    @SerializedName("student_topic")
    private String stu_topic = "";
    @SerializedName("student_target")
    private String stu_target = "";
    //------------------------------
    //for profesor details
    @SerializedName("professor_university")
    private String pro_university = "";
    @SerializedName("professor_degree")
    private String pro_level = "";
    @SerializedName("professor_topic")
    private String pro_topic = "";
    @SerializedName("professor_target")
    private String pro_target = "";
    //------------------------------
    // for prject details
    @SerializedName("project_name")
    private String projectname = "";
    @SerializedName("project_start_date")
    private String projectstartdate = "";
    @SerializedName("project_organizations_id")
    private int projectorganid = 0;
    @SerializedName("project_user_role")
    private String projectuserrole = "";
    @SerializedName("project_km")
    private String projectkm = "";
    //-----------------------------------------
    public User() {
    }
    //for casual
    public User(String firstname, String lastname, String username, String email, String phone, String password, String usertype, String userjob) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.usertype = MyUtiles.USER_TYPE_p2e.get(usertype);
        this.userjob = userjob;
    }

    //for pro setting
    public User(String firstname, String lastname, String username, String email, String phone, String password, String usertype, String userjob, String pro_university, String pro_level, String pro_topic, String pro_target) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.usertype = MyUtiles.USER_TYPE_p2e.get(usertype);
        this.userjob = userjob;
        this.pro_university = pro_university;
        this.pro_level = MyUtiles.PersianToEnglish.get(pro_level);
        this.pro_topic = pro_topic;
        this.pro_target = MyUtiles.PersianToEnglish.get(pro_target);
    }
    //for project setting
    public User(int i , String firstname, String lastname, String username, String email, String phone, String password, String usertype, String userjob, int projectorganid, String projectstartdate, String projectname, String projectuserrole, String projectkm) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.usertype = MyUtiles.USER_TYPE_p2e.get(usertype);
        this.userjob = userjob;
        this.projectname = projectname;
        this.projectstartdate = projectstartdate;
        this.projectorganid = projectorganid;
        this.projectkm = projectkm;
        this.projectuserrole=MyUtiles.PersianToEnglish.get(projectuserrole);
    }
    //for student setting
    public User(String firstname, String lastname, String username, String email, String phone, String password, String usertype, String userjob, String stu_university, String stu_level, String stu_supervisor, String stu_topic, String stu_target) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.usertype = MyUtiles.USER_TYPE_p2e.get(usertype);
        this.userjob = userjob;
        this.stu_university = stu_university;
        this.stu_level = MyUtiles.PersianToEnglish.get(stu_level);
        this.stu_supervisor = stu_supervisor;
        this.stu_topic = stu_topic;
        this.stu_target = MyUtiles.PersianToEnglish.get(stu_target);
    }
    //for all setting
    public User(String firstname, String lastname, String username, String email, String phone, String password, String usertype, String userjob,
                String stu_university, String stu_level, String stu_supervisor, String stu_topic, String stu_target, String pro_university,
                String pro_level, String pro_topic, String pro_target, String projectname, String projectstartdate, int projectorganid,
                String projectuserrole, String projectkm) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.usertype = MyUtiles.USER_TYPE_p2e.get(usertype);
        this.userjob = userjob;
        this.stu_university = stu_university;
        this.stu_level = MyUtiles.PersianToEnglish.get(stu_level);
        this.stu_supervisor = stu_supervisor;
        this.stu_topic = stu_topic;
        this.stu_target = MyUtiles.PersianToEnglish.get(stu_target);
        this.pro_university = pro_university;
        this.pro_level = MyUtiles.PersianToEnglish.get(pro_level);
        this.pro_topic = pro_topic;
        this.pro_target = MyUtiles.PersianToEnglish.get(pro_target);
        this.projectname = projectname;
        this.projectstartdate = projectstartdate;
        this.projectorganid = projectorganid;
        this.projectuserrole = MyUtiles.PersianToEnglish.get(projectuserrole);;
        this.projectkm = projectkm;
    }
    //-----------------------------------------------
    public static User GetUserInfo(){
        User user = new User();
        user.setFirstname(InitActivity.getUserFirstName());
        user.setLastname(InitActivity.getUserFamilyName());
        user.setEmail(InitActivity.getUserEmail());
        user.setPassword(InitActivity.getPassword());
        user.setPhone(InitActivity.getUserPhone());
        user.setUsertype(InitActivity.getUserType());
        user.setUserjob(InitActivity.getUserJob());
        //-----------------------------
        switch (user.getUsertype()){
            case "regular":
                break;
            case "student":
                user.setStu_level(InitActivity.getStudent_level());
                user.setStu_supervisor(InitActivity.getStudent_supervisor());
                user.setStu_target(InitActivity.getStudent_target());
                user.setStu_topic(InitActivity.getStudent_topic());
                user.setStu_university(InitActivity.getStudent_university());
                break;
            case "professor":
                user.setPro_level(InitActivity.getProfessor_degree());
                user.setPro_target(InitActivity.getProfessor_target());
                user.setPro_topic(InitActivity.getProfessor_topic());
                user.setPro_university(InitActivity.getProfessor_university());
                break;
            case "project":
                user.setProjectname(InitActivity.getProject_name());
                user.setProjectkm(InitActivity.getProject_km()+"");
                user.setProjectorganid(InitActivity.getProject_organizations_id());
                user.setProjectstartdate(InitActivity.getProject_start_date()+"");
                user.setProjectuserrole(InitActivity.getProject_user_role());
                break;
        }
        return user;
    }
    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getUsertype() {
        return usertype;
    }

    public String getUserjob() {
        return userjob;
    }

    public String getStu_university() {
        return stu_university;
    }

    public String getStu_level() {
        return stu_level;
    }

    public String getStu_supervisor() {
        return stu_supervisor;
    }

    public String getStu_topic() {
        return stu_topic;
    }

    public String getStu_target() {
        return stu_target;
    }

    public String getPro_university() {
        return pro_university;
    }

    public String getPro_level() {
        return pro_level;
    }

    public String getPro_topic() {
        return pro_topic;
    }

    public String getPro_target() {
        return pro_target;
    }

    public String getProjectname() {
        return projectname;
    }

    public String getProjectstartdate() {
        return projectstartdate;
    }

    public int getProjectorganid() {
        return projectorganid;
    }

    public String getProjectuserrole() {
        return projectuserrole;
    }

    public String getProjectkm() {
        return projectkm;
    }


    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public void setUserjob(String userjob) {
        this.userjob = userjob;
    }

    public void setStu_university(String stu_university) {
        this.stu_university = stu_university;
    }

    public void setStu_level(String stu_level) {
        this.stu_level = stu_level;
    }

    public void setStu_supervisor(String stu_supervisor) {
        this.stu_supervisor = stu_supervisor;
    }

    public void setStu_topic(String stu_topic) {
        this.stu_topic = stu_topic;
    }

    public void setStu_target(String stu_target) {
        this.stu_target = stu_target;
    }

    public void setPro_university(String pro_university) {
        this.pro_university = pro_university;
    }

    public void setPro_level(String pro_level) {
        this.pro_level = pro_level;
    }

    public void setPro_topic(String pro_topic) {
        this.pro_topic = pro_topic;
    }

    public void setPro_target(String pro_target) {
        this.pro_target = pro_target;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public void setProjectstartdate(String projectstartdate) {
        this.projectstartdate = projectstartdate;
    }

    public void setProjectorganid(int projectorganid) {
        this.projectorganid = projectorganid;
    }

    public void setProjectuserrole(String projectuserrole) {
        this.projectuserrole = projectuserrole;
    }

    public void setProjectkm(String projectkm) {
        this.projectkm = projectkm;
    }

}
