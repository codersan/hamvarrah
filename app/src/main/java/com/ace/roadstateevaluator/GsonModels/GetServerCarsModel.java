package com.ace.roadstateevaluator.GsonModels;

import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.DB.CarDB;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

public class GetServerCarsModel {
    //---------------------------
    private static HashMap<String,ArrayList<CarModels>> cars;

    static {
        if(cars==null){cars=new HashMap<>();}
    }
    //-------------------------
    public GetServerCarsModel() {

    }

    public static HashMap<String, ArrayList<CarModels>> getCars() {
        return cars;
    }

    public static void setCars(HashMap<String, ArrayList<CarModels>> cars) {
        GetServerCarsModel.cars = cars;
    }
}
