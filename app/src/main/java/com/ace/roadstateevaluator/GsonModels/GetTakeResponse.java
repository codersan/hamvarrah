package com.ace.roadstateevaluator.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetTakeResponse {
    @SerializedName("survay_most_frequent_velocity_range")
    private String freqvelocity="";
    @SerializedName("survay_start")
    private String start="";
    @SerializedName("survay_description")
    private String desc="";
    @SerializedName("survay_id")
    private int id=0;
    @SerializedName("survay_user_id")
    private int userid=0;
    @SerializedName("survay_car_id")
    private int carid=0;
    @SerializedName("survay_max_lat")
    private float maxlat=0;
    @SerializedName("survay_min_lat")
    private float minlat=0;
    @SerializedName("survay_max_lon")
    private float maxlon=0;
    @SerializedName("survay_min_lon")
    private float minlon=0;
    @SerializedName("survay_total_len")
    private int totallen=0;
    @SerializedName("survay_valid_total_len")
    private int validtotallen=0;
    @SerializedName("survay_max_velocity")
    private int maxvelocity=0;
    @SerializedName("survay_mean_velocity")
    private int meanvelocity=0;
    @Expose(serialize = false, deserialize = false)
    private String carname="";
    public String getFreqvelocity() {
        return freqvelocity;
    }

    public String getStart() {
        return start;
    }

    public String getDesc() {
        return desc;
    }

    public int getId() {
        return id;
    }

    public int getUserid() {
        return userid;
    }

    public int getCarid() {
        return carid;
    }

    public float getMaxlat() {
        return maxlat;
    }

    public float getMinlat() {
        return minlat;
    }

    public float getMaxlon() {
        return maxlon;
    }

    public float getMinlon() {
        return minlon;
    }

    public int getTotallen() {
        return totallen;
    }

    public int getValidtotallen() {
        return validtotallen;
    }

    public int getMaxvelocity() {
        return maxvelocity;
    }

    public int getMeanvelocity() {
        return meanvelocity;
    }

    public void setFreqvelocity(String freqvelocity) {
        this.freqvelocity = freqvelocity;
    }

    public void setStart(String start) {
        String[] ss=start.split("T");
        start=ss[0]+"\n"+ss[1].substring(0,5);
        this.start = start;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public void setMaxlat(float maxlat) {
        this.maxlat = maxlat;
    }

    public void setMinlat(float minlat) {
        this.minlat = minlat;
    }

    public void setMaxlon(float maxlon) {
        this.maxlon = maxlon;
    }

    public String getCarname() {
        return carname;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public void setMinlon(float minlon) {
        this.minlon = minlon;
    }

    public void setTotallen(int totallen) {
        this.totallen = totallen;
    }

    public void setValidtotallen(int validtotallen) {
        this.validtotallen = validtotallen;
    }

    public void setMaxvelocity(int maxvelocity) {
        this.maxvelocity = maxvelocity;
    }

    public void setMeanvelocity(int meanvelocity) {
        this.meanvelocity = meanvelocity;
    }
}
