package com.ace.roadstateevaluator.GsonModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Car {
    @Expose(serialize = false, deserialize = true)
    @SerializedName("car_id")
    private int carid=0;
    @Expose(serialize = false, deserialize = false)
    private int carlength=0;
    @SerializedName("car_model_id")
    private int modelid=0;
    @SerializedName("car_creation_year")
    private int creatyear=0;
    @SerializedName("car_usage_km")
    private int usagekm=0;
    @SerializedName("car_user_id")
    private int userid=0;
    @SerializedName("car_name")
    private String carname="";
    @Expose(serialize = false, deserialize = false)
    private boolean selected;
    //--------------------------------------
    //keynames
    @Expose(serialize = false, deserialize = false)
    public static String KeyId="carid";
    @Expose(serialize = false, deserialize = false)
    public static String Keylength="cartotallength";
    @Expose(serialize = false, deserialize = false)
    public static String KeyName="carname";
    @Expose(serialize = false, deserialize = false)
    public static String KeyCreation="carcreation";
    @Expose(serialize = false, deserialize = false)
    public static String KeyUsage="caruage";
    @Expose(serialize = false, deserialize = false)
    public static String KeySelect="carselect";

    //----------------------------
    public Car() {
    }
    public Car(int modelid, int creatyear, int usagekm, int userid, String carname,boolean selected) {
        this.modelid = modelid;
        this.creatyear = creatyear;
        this.usagekm = usagekm;
        this.userid = userid;
        this.carname = carname;
        this.selected=selected;
    }

    public void setModelid(int modelid) {
        this.modelid = modelid;
    }

    public void setCreatyear(int creatyear) {
        this.creatyear = creatyear;
    }

    public void setUsagekm(int usagekm) {
        this.usagekm = usagekm;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setCarname(String carname) {
        this.carname = carname;
    }

    public int getModelid() {
        return modelid;
    }

    public int getCreatyear() {
        return creatyear;
    }

    public int getUsagekm() {
        return usagekm;
    }

    public int getUserid() {
        return userid;
    }

    public String getCarname() {
        return carname;
    }

    public boolean getSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    public void setCarid(int carid) {
        this.carid = carid;
    }

    public int getCarid() {
        return carid;
    }
    public int getCarlength() {
        return carlength;
    }

    public void setCarlength(int carlength) {
        this.carlength = carlength;
    }
}
