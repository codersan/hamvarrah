package com.ace.roadstateevaluator.GsonModels;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("username")
    private String username="";
    @SerializedName("access")
    private String access="";
    @SerializedName("refresh")
    private String refresh="";
   //----------------------------------
    public String getUsername() {
        return username;
    }

    public String getAccess() {
        return access;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getRefresh() {
        return refresh;
    }

    public int getUserid() {
        return userid;
    }

    @SerializedName("user_id")
    private int userid;
}
