package com.ace.roadstateevaluator.fragments.login;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ace.roadstateevaluator.Activity.LoginActivity;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;


public class SingUpFragment extends Fragment {

    private View root;
    private LoginActivity activity;
    private AutoCompleteTextView type;
    String[] choose =new String[]{"عادی","دانشجو","هیات علمی","سازمانی"};
    public static SingUpFragment newInstance(LoginActivity activity) {
        return new SingUpFragment(activity);
    }

    public SingUpFragment(LoginActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root=inflater.inflate(R.layout.signup_fragment, container, false);



        init();

        Log.d("QQQQQQQQQQQQ","sin");
        return root;
    }

    private void init() {
        final int[] type_index = {-1};
        final TextInputEditText fistname=root.findViewById(R.id.ed_sign1_firstname);
        final TextInputLayout fistname_l=root.findViewById(R.id.ed_sign1_firstname_l);

        final TextInputEditText lastname=root.findViewById(R.id.ed_sign1_lastname);
        final TextInputEditText username=root.findViewById(R.id.ed_sign1_username);
        final TextInputEditText phonenumber=root.findViewById(R.id.ed_sign1_phonenumber);
        final TextInputEditText email=root.findViewById(R.id.ed_sign1_email);
        final TextInputEditText pass=root.findViewById(R.id.ed_sign1_password);

        type=root.findViewById(R.id.ed_sign1_usertype);
        ArrayAdapter adapter=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,choose);//String[]{"عادی","دانشجو","هیات علمی","سازمانی"});
        type.setAdapter(adapter);

        type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                type_index[0] =i;
            }
        });


        final Button next=root.findViewById(R.id.b_signup_1);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ready=true;
                if (fistname.getText().toString().isEmpty()){
                    fistname_l.setError("ضروری");
                    ready=false;

                }
                if (lastname.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) lastname.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;

                }
                if (username.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) username.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;

                }
                if (phonenumber.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) phonenumber.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;

                }else {
                    if (!MyUtiles.CheckPhone(phonenumber.getText().toString())){
                        final TextInputLayout layout= (TextInputLayout) phonenumber.getParent().getParent();
                        layout.setError("شماره نامعتبر");
                        ready=false;

                    }
                }
                if (email.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) email.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;

                }else {
                    if (!MyUtiles.CheckEmail(email.getText().toString())){
                        final TextInputLayout layout= (TextInputLayout) email.getParent().getParent();
                        layout.setError("ایمیل نامعتبر");
                        ready=false;

                    }
                }
                if (pass.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) pass.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;

                }


                if (type_index[0]==-1){
                    final TextInputLayout layout= (TextInputLayout) type.getParent().getParent();
                    layout.setError("یک نوع را انتخاب کنید");
                    ready=false;

                }
                if (pass.getText().toString().length()<5){
                    final TextInputLayout layout= (TextInputLayout) pass.getParent().getParent();
                    layout.setError("رمز کوتاه است");
                    ready=false;

                }

                if (ready){

                    activity.uservalues=new String[]{fistname.getText().toString(),lastname.getText().toString(),username.getText().toString(),phonenumber.getText().toString(),
                            email.getText().toString(),pass.getText().toString(), MyUtiles.USER_TYPE_p2e.get(choose[type_index[0]])};
                    activity.go_signup2(type_index[0]);

                }else {
                    Snackbar.make(view,"لطفا مقادیر معتبر وارد نمایید",Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        if (type!=null) {
            type.setText("");
            ArrayAdapter adapter = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, choose);//String[]{"عادی","دانشجو","هیات علمی","سازمانی"});
            type.setAdapter(adapter);
        }
    }
}