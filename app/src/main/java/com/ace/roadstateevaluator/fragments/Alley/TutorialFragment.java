package com.ace.roadstateevaluator.fragments.Alley;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.R;

public class TutorialFragment extends Fragment {

    private View root;


    public static TutorialFragment newInstance() {
        return new TutorialFragment();
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root=inflater.inflate(R.layout.tutorial_fragment, container, false);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.mainActivity.should_this_shit_be_back_button=true;

    }
}