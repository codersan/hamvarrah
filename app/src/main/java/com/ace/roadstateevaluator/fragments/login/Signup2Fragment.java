package com.ace.roadstateevaluator.fragments.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ace.roadstateevaluator.Activity.LoginActivity;
import com.ace.roadstateevaluator.GsonModels.GetOrganList;
import com.ace.roadstateevaluator.GsonModels.User;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Map;

public class Signup2Fragment extends Fragment {

    private View root;
    private int usertype=-1;
    private Button create;
    private LoginActivity activity;
    private String[] user_info;
    private String sazman_id;

    public static Signup2Fragment newInstance(int usertype,LoginActivity activity) {
        return new Signup2Fragment(usertype,activity);
    }

    public Signup2Fragment(int usertype,LoginActivity activity) {
        this.usertype = usertype;
        this.activity=activity;
        user_info=activity.uservalues;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {




        switch (usertype){
            case 0:
                root=inflater.inflate(R.layout.signup_fragment_casual, container, false);
                create=root.findViewById(R.id.b_sign2);

                initcas();
                break;
            case 1:
                root=inflater.inflate(R.layout.signup_fragment_student, container, false);
                create=root.findViewById(R.id.b_sign2);
                initstu();
                break;
            case 2:
                root=inflater.inflate(R.layout.signup_fragment_heiat, container, false);
                create=root.findViewById(R.id.b_sign2);
                initpro();
                break;
            case 3:
                root=inflater.inflate(R.layout.signup_fragment_sazmani, container, false);
                create=root.findViewById(R.id.b_sign2);
                initsaz();
                break;
        }

        init();

        return root;
    }

    private void init() {
    }

    private void send_req(User user,View view){
        ProgressBar pb=root.findViewById(R.id.pb_signup);
        pb.setVisibility(View.VISIBLE);
        create.setActivated(false);
        boolean b=MyUtiles.SendSignUp(user,view.getContext(),create,pb,getActivity());



    }

    private void initcas() {
        final EditText job=root.findViewById(R.id.ed_sign2cas_job);




        final CheckBox cb=root.findViewById(R.id.cb_sign2);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ready=true;
                if (!cb.isChecked()){
                    Toast.makeText(view.getContext(), "برای استفاده از برنامه میبایست قوانین و مقررات را بپزیرید", Toast.LENGTH_SHORT).show();
                    ready=false;
                }
                if (job.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) job.getParent().getParent();
                    layout.setError("ضروری");
                }


                if (ready){
                    User user=new User(user_info[0],user_info[1],user_info[2],user_info[4],user_info[3],user_info[5],"عادی",job.getText().toString());
                    send_req(user,view);
                }

            }
        });

    }

    private void initstu() {
        final int[] spinner_values=new int[]{-1,-1};//  [0]=level   [1]=goal

        final EditText uniname=root.findViewById(R.id.ed_sign2student_uniname);
        final EditText teacher=root.findViewById(R.id.ed_sign2student_teacher);
        final EditText subject=root.findViewById(R.id.ed_sign2student_subject);

        final AutoCompleteTextView level=root.findViewById(R.id.ed_sign2student_level);
        final String[] level_values=new String[]{"کارشناسی","کارشناسی ارشد","دکتری"};
        ArrayAdapter ad1=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,level_values);
        level.setAdapter(ad1);
        level.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[0] =i;
            }
        });

        final AutoCompleteTextView goal=root.findViewById(R.id.ed_sign2student_goal);
        final String[] goal_values=new String[]{"پروژه درسی","مقاله علمی","پایان نامه","سایر"};
        ArrayAdapter ad2=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,goal_values);
        goal.setAdapter(ad2);
        goal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] =i;
            }
        });



        //init button
        final CheckBox cb=root.findViewById(R.id.cb_sign2);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ready=true;
                if (!cb.isChecked()){
                    Toast.makeText(view.getContext(), "برای استفاده از برنامه میبایست قوانین و مقررات را بپزیرید", Toast.LENGTH_SHORT).show();
                    ready=false;
                }
                if (uniname.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) uniname.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }

                if (teacher.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) teacher.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (subject.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) subject.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (spinner_values[0]==-1){
                    final TextInputLayout layout= (TextInputLayout) level.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (spinner_values[1]==-1){
                    final TextInputLayout layout= (TextInputLayout) goal.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }


                if (ready){
                    User user=new User(user_info[0],user_info[1],user_info[2],user_info[4],user_info[3],user_info[5],"دانشجو","دانشجو",uniname.getText().toString(),level_values[spinner_values[0]],teacher.getText().toString(),subject.getText().toString(),goal_values[spinner_values[1]]);
                    send_req(user,view);
                }
            }
        });
    }

    private void initpro() {
        final int[] spinner_values=new int[]{-1,-1};//  [0]=level   [1]=goal

        final EditText uniname=root.findViewById(R.id.ed_sign2heyat_uniname);
        final EditText subject=root.findViewById(R.id.ed_sign2heyat_subject);

        final AutoCompleteTextView level=root.findViewById(R.id.ed_sign2heyat_level);
        final String[] level_values=new String[]{"مربی","استادیار","دانشیار","استاد"};
        ArrayAdapter ad1=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,level_values);
        level.setAdapter(ad1);
        level.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[0] =i;
            }
        });

        final AutoCompleteTextView goal=root.findViewById(R.id.ed_sign2heyat_goal);
        final String[] goal_values=new String[]{"مقاله علمی","پایان نامه","سایر"};
        ArrayAdapter ad2=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,goal_values);
        goal.setAdapter(ad2);
        goal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] =i;
            }
        });



        final CheckBox cb=root.findViewById(R.id.cb_sign2);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ready=true;
                if (!cb.isChecked()){
                    Toast.makeText(view.getContext(), "برای استفاده از برنامه میبایست قوانین و مقررات را بپزیرید", Toast.LENGTH_SHORT).show();
                    ready=false;
                }
                if (uniname.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) uniname.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (subject.getText().toString().isEmpty()){
                    final TextInputLayout layout= (TextInputLayout) subject.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (spinner_values[0]==-1){
                    final TextInputLayout layout= (TextInputLayout) level.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (spinner_values[1]==-1){
                    final TextInputLayout layout= (TextInputLayout) goal.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }

                if (ready){
                    User user=new User(user_info[0],user_info[1],user_info[2],user_info[4],user_info[3],user_info[5],"هیات علمی","هیات علمی",uniname.getText().toString(),level_values[spinner_values[0]],subject.getText().toString(),goal_values[spinner_values[1]]);
                    send_req(user,view);
                }

            }
        });
    }

    private void initsaz() {
        final int[] spinner_values=new int[]{-1,-1,-1};//  [0]=ins name   [1]=proj name [2]=role


        //project names
        final AutoCompleteTextView proj_name=root.findViewById(R.id.ed_sign2inst_projectname);
        final ArrayList<String> projects_names = new ArrayList<>();
        final ArrayAdapter ad1=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,projects_names);
        proj_name.setAdapter(ad1);
        proj_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] =i;
            }
        });


        //sazman names
        final AutoCompleteTextView insss_name=root.findViewById(R.id.ed_sign2inst_name);
        final ArrayList<String> institutions_names =new ArrayList<>();
        ArrayAdapter names_Adapter=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,institutions_names);
        insss_name.setAdapter(names_Adapter);
        insss_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[0] =i;
                spinner_values[1]=-1;


                Map<String, ArrayList<String>> map=GetOrganList.getOrgans();
                projects_names.clear();
                ArrayList<String> sinas_shitty_list=map.get(institutions_names.get(spinner_values[0]));
                for (int j = 1; j < sinas_shitty_list.size(); j++) {
                    projects_names.add(sinas_shitty_list.get(j));

                }
                sazman_id=sinas_shitty_list.get(0);


                ad1.notifyDataSetChanged();
            }
        });
        final TextInputLayout insss_name_l=root.findViewById(R.id.ed_sign2inst_name_l);





        //roles
        final AutoCompleteTextView user_role=root.findViewById(R.id.ed_sign2inst_role);
        final String[] roles = new String[]{"مسئول سازمان","مسئول پروژه","برداشت کننده داده"};
        ArrayAdapter ad2=new ArrayAdapter(root.getContext(),R.layout.support_simple_spinner_dropdown_item,roles);
        user_role.setAdapter(ad2);
        user_role.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[2] =i;
            }
        });


        projects_names.clear();
        MyUtiles.GetOrgan(getActivity(),names_Adapter,institutions_names,proj_name,user_role,insss_name);



        final CheckBox cb=root.findViewById(R.id.cb_sign2);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ready=true;
                if (!cb.isChecked()){
                    Toast.makeText(view.getContext(), "برای استفاده از برنامه میبایست قوانین و مقررات را بپزیرید", Toast.LENGTH_SHORT).show();
                    ready=false;
                }

                if (spinner_values[0]==-1){
                    final TextInputLayout layout= (TextInputLayout) insss_name_l.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (spinner_values[1]==-1){
                    final TextInputLayout layout= (TextInputLayout) proj_name.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }
                if (spinner_values[2]==-1){
                    final TextInputLayout layout= (TextInputLayout) user_role.getParent().getParent();
                    layout.setError("ضروری");
                    ready=false;
                }

                if (ready){
                    User user=new User(-1,user_info[0],user_info[1],user_info[2],user_info[4],user_info[3],user_info[5],"سازمانی","سازمانی",Integer.parseInt(sazman_id),"",projects_names.get(spinner_values[1]),roles[spinner_values[2]],"100");
                    send_req(user,view);
                }

            }
        });
    }


}