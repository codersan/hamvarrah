package com.ace.roadstateevaluator.fragments.takes;


import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.ace.roadstateevaluator.GsonModels.GetTakeList;
import com.ace.roadstateevaluator.GsonModels.GetTakeResponse;
import com.ace.roadstateevaluator.GsonModels.TakeSession;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.Vars;

import java.util.ArrayList;

public class RVA_Takes extends RecyclerView.Adapter<RVA_Takes.VH> {

    private ArrayList<GetTakeResponse> all;

    public RVA_Takes(ArrayList<GetTakeResponse> all) {
        this.all = all;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.eachtake, parent, false);


        return new VH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.bind(position);

    }

    @Override
    public int getItemCount() {
        return all.size();
    }

    public class VH extends RecyclerView.ViewHolder {

        TextView des,dis,model,date;

        public VH(@NonNull final View itemView) {
            super(itemView);

            des=itemView.findViewById(R.id.tv_eachtake_des);
            dis=itemView.findViewById(R.id.tv_eachtake_dis);
            model=itemView.findViewById(R.id.tv_eachtake_car);
            date=itemView.findViewById(R.id.tv_eachtake_date);

        }

        public void bind(final int i) {

            GetTakeResponse take=all.get(i);

            des.setText(take.getDesc());
            dis.setText(take.getTotallen()+"m");
            date.setText(take.getStart());
            model.setText(take.getCarname());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    show_dialog(view.getContext(),i);
                }
            });

        }



    }

    public void show_dialog(Context context,int i){
        //create new car
        final Dialog dialog = new Dialog(context,R.style.PauseDialog);
        LayoutInflater inflater = LayoutInflater.from(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View mainview = inflater.inflate(R.layout.view_take, null);
        dialog.setContentView(mainview);



        Button b=mainview.findViewById(R.id.b_edittake_b);
        TextView ed=mainview.findViewById(R.id.ed_edittake_des);
        ed.setText(all.get(i).getDesc());
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });



        //make dialog BIG
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (Vars.width );
        lp.height = (int) (Vars.height * 0.5);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.backdialoge));


        Window window = dialog.getWindow();
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);

        dialog.show();

    }

}
