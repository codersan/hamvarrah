package com.ace.roadstateevaluator.fragments.Alley;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.GsonModels.GetOrganList;
import com.ace.roadstateevaluator.GsonModels.User;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.RetrofitResponseListener;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;

public class EditProfileFragment extends Fragment {

    boolean ready;
    private View root;
    EditText[] usual_fields;
    String[] usual_user_info_val;

    private String sazman_id;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        MainActivity.mainActivity.should_this_shit_be_back_button=true;


        root = inflater.inflate(R.layout.editprofile_fragment, container, false);


        fill_overall_details();

        return root;
    }


    private void fill_overall_details() {

        usual_user_info_val = new String[]{InitActivity.getUserFirstName(), InitActivity.getUserFamilyName(), InitActivity.getUserPhone(), InitActivity.getUserEmail()};

        //get the mother view
        LinearLayout holder = root.findViewById(R.id.ll_editprofile);
        //get info text array
        String[] usual_info = root.getContext().getResources().getStringArray(R.array.userinfo_all_edit);
        //get an Inflater
        LayoutInflater vi = (LayoutInflater) root.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //fields
        usual_fields = new EditText[usual_info.length];

        //foreach text in the array add a view to the mother
        for (int i = 0; i < usual_info.length; i++) {
            ConstraintLayout child;

            child = (ConstraintLayout) vi.inflate(R.layout.child3, null);
            ((TextInputLayout) child.findViewById(R.id.tl_ch3)).setHint(usual_info[i]);
            EditText ed = child.findViewById(R.id.ed_editprofile);
            ed.setText(usual_user_info_val[i]);

            usual_fields[i] = ed;

            holder.addView(child);

        }


        //get description text array
        String[] specific_info = new String[0];
        String[] user_specific_info_val = new String[0];

        //fill the rest based on USER TYPE
        int type = InitActivity.UserTypeIndex;


        switch (type) {
            case 0:

                fill_rest_casual(usual_user_info_val);
                break;
            case 1:
                specific_info = root.getContext().getResources().getStringArray(R.array.userinfo_student);
                user_specific_info_val = new String[]{InitActivity.getStudent_university(), InitActivity.getStudent_level(), InitActivity.getStudent_supervisor(), InitActivity.getStudent_topic(), InitActivity.getStudent_target()};

                fill_rest_student(vi, specific_info, user_specific_info_val, holder, usual_user_info_val);
                break;
            case 2:
                specific_info = root.getContext().getResources().getStringArray(R.array.userinfo_heiat);
                user_specific_info_val = new String[]{InitActivity.getProfessor_university(), InitActivity.getProfessor_degree(), InitActivity.getProfessor_topic(), InitActivity.getProfessor_target()};

                fill_rest_heiat(vi, specific_info, user_specific_info_val, holder, usual_user_info_val);
                break;
            case 3:
                specific_info = root.getContext().getResources().getStringArray(R.array.userinfo_sazmani);
                user_specific_info_val = new String[]{InitActivity.getProject_organizations_id() + "", InitActivity.getProject_name(), InitActivity.getProject_user_role()};

                fill_rest_sazman(vi, specific_info, user_specific_info_val, holder, usual_user_info_val);
                break;


        }


    }

    private void fill_rest_student(LayoutInflater vi, String[] specific_info, String[] user_specific_info_val, LinearLayout holder, final String[] usual_user_info_val) {


//        ConstraintLayout child = (ConstraintLayout) vi.inflate(R.layout.child3, null);
//        ((TextInputLayout) child.findViewById(R.id.tl_ch3)).setHint();
//        ((EditText) child.findViewById(R.id.ed_editprofile)).setText(user_specific_info_val[i]);
//        holder.addView(child);
//
//
//
//        ConstraintLayout child = (ConstraintLayout) vi.inflate(R.layout.child4, null);
//        ((TextInputLayout) child.findViewById(R.id.tl_ch4)).setHint();
//        ((AutoCompleteTextView) child.findViewById(R.id.ed_editprofile_1)).setText(user_specific_info_val[i]);
//        holder.addView(child);


        final int[] spinner_values = new int[]{-1, -1};//  [0]=level   [1]=goal


        ConstraintLayout uniname0 = (ConstraintLayout) vi.inflate(R.layout.child3, null);
        ((TextInputLayout) uniname0.findViewById(R.id.tl_ch3)).setHint(specific_info[0]);
        final EditText uniname = ((EditText) uniname0.findViewById(R.id.ed_editprofile));
        uniname.setText(user_specific_info_val[0]);
        holder.addView(uniname0);

        ConstraintLayout teacher0 = (ConstraintLayout) vi.inflate(R.layout.child3, null);
        ((TextInputLayout) teacher0.findViewById(R.id.tl_ch3)).setHint(specific_info[2]);
        final EditText teacher = ((EditText) teacher0.findViewById(R.id.ed_editprofile));
        teacher.setText(user_specific_info_val[2]);
        holder.addView(teacher0);

        ConstraintLayout subject0 = (ConstraintLayout) vi.inflate(R.layout.child3, null);
        ((TextInputLayout) subject0.findViewById(R.id.tl_ch3)).setHint(specific_info[3]);
        final EditText subject = ((EditText) subject0.findViewById(R.id.ed_editprofile));
        subject.setText(user_specific_info_val[3]);
        holder.addView(subject0);

        //spinner ones

        ConstraintLayout level0 = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) level0.findViewById(R.id.tl_ch4)).setHint(specific_info[1]);
        final AutoCompleteTextView level = ((AutoCompleteTextView) level0.findViewById(R.id.ed_editprofile_1));
//        level.setText(user_specific_info_val[1]);

        final String[] level_values = new String[]{"کارشناسی", "کارشناسی ارشد", "دکتری"};
        ArrayAdapter ad1 = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, level_values);
        level.setAdapter(ad1);
        level.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[0] = i;
            }
        });
        holder.addView(level0);


        ConstraintLayout goal0 = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) goal0.findViewById(R.id.tl_ch4)).setHint(specific_info[4]);
        final AutoCompleteTextView goal = ((AutoCompleteTextView) goal0.findViewById(R.id.ed_editprofile_1));
//        goal.setText(user_specific_info_val[4]);
        final String[] goal_values = new String[]{"پروژه درسی", "مقاله علمی", "پایان نامه", "سایر"};
        ArrayAdapter ad2 = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, goal_values);
        goal.setAdapter(ad2);
        goal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] = i;
            }
        });
        holder.addView(goal0);


        //init button
        Button create = root.findViewById(R.id.b_editprofile);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                check_usual_fields();
                if (uniname.getText().toString().isEmpty()) {
                    final TextInputLayout layout = (TextInputLayout) uniname.getParent().getParent();
                    layout.setError("ضروری");
                    ready = false;
                }

                if (teacher.getText().toString().isEmpty()) {
                    final TextInputLayout layout = (TextInputLayout) teacher.getParent().getParent();
                    layout.setError("ضروری");
                    ready = false;
                }
                if (subject.getText().toString().isEmpty()) {
                    final TextInputLayout layout = (TextInputLayout) subject.getParent().getParent();
                    layout.setError("ضروری");
                    ready = false;
                }
                if (spinner_values[0] == -1) {
                    ready = false;
                }
                if (spinner_values[1] == -1) {
                    ready = false;
                }

                if (ready) {
                    User user = new User(usual_user_info_val[0], usual_user_info_val[1], InitActivity.getUsername(), usual_user_info_val[3], usual_user_info_val[2], InitActivity.getPassword(), "دانشجو", "دانشجو", uniname.getText().toString(), level_values[spinner_values[0]], teacher.getText().toString(), subject.getText().toString(), goal_values[spinner_values[1]]);
                    try {
                        send_req(user, view);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(view.getContext(), "لطفا همه مقادیر را وارد کنید", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    private void send_req(User user, final View view) throws JSONException {
        ProgressBar pb = root.findViewById(R.id.pb_edit);
        pb.setVisibility(View.VISIBLE);
        MyUtiles.UpdateUserInfo(user, view.getContext(), new RetrofitResponseListener() {
            final View viewf = view;

            @Override
            public void onSuccess() {
                startActivity(new Intent(viewf.getContext(), MainActivity.class));
                Toast.makeText(viewf.getContext(), "حساب کاربری با موفقیت بروزرسانی شد", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure() {
                Toast.makeText(viewf.getContext(), "خطا", Toast.LENGTH_SHORT).show();
            }
        });
        pb.setVisibility(View.INVISIBLE);

    }

    private void fill_rest_heiat(LayoutInflater vi, String[] specific_info, String[] user_specific_info_val, LinearLayout holder, final String[] usual_user_info_val) {
        final int[] spinner_values = new int[]{-1, -1};//  [0]=level   [1]=goal

        ConstraintLayout uniname0 = (ConstraintLayout) vi.inflate(R.layout.child3, null);
        ((TextInputLayout) uniname0.findViewById(R.id.tl_ch3)).setHint(specific_info[0]);
        final EditText uniname = ((EditText) uniname0.findViewById(R.id.ed_editprofile));
        uniname.setText(user_specific_info_val[0]);
        holder.addView(uniname0);

        ConstraintLayout subject0 = (ConstraintLayout) vi.inflate(R.layout.child3, null);
        ((TextInputLayout) subject0.findViewById(R.id.tl_ch3)).setHint(specific_info[2]);
        final EditText subject = ((EditText) subject0.findViewById(R.id.ed_editprofile));
        subject.setText(user_specific_info_val[2]);
        holder.addView(subject0);


        ConstraintLayout level0 = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) level0.findViewById(R.id.tl_ch4)).setHint(specific_info[1]);
        final AutoCompleteTextView level = ((AutoCompleteTextView) level0.findViewById(R.id.ed_editprofile_1));
//        level.setText(user_specific_info_val[1]);

        final String[] level_values = new String[]{"مربی", "استادیار", "دانشیار", "استاد"};
        ArrayAdapter ad1 = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, level_values);
        level.setAdapter(ad1);
        level.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[0] = i;
            }
        });
        holder.addView(level0);


        ConstraintLayout goal0 = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) goal0.findViewById(R.id.tl_ch4)).setHint(specific_info[3]);
        final AutoCompleteTextView goal = ((AutoCompleteTextView) goal0.findViewById(R.id.ed_editprofile_1));
//        goal.setText(user_specific_info_val[3]);

        final String[] goal_values = new String[]{"مقاله علمی", "پایان نامه", "سایر"};
        ArrayAdapter ad2 = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, goal_values);
        goal.setAdapter(ad2);
        goal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] = i;
            }
        });
        holder.addView(goal0);


        Button create = root.findViewById(R.id.b_editprofile);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                check_usual_fields();
                if (uniname.getText().toString().isEmpty()) {
                    uniname.setError("ضروری");
                    ready = false;
                }
                if (subject.getText().toString().isEmpty()) {
                    subject.setError("ضروری");
                    ready = false;
                }
                if (spinner_values[0] == -1) {
                    ready = false;
                }
                if (spinner_values[1] == -1) {
                    ready = false;
                }


                if (ready) {
                    User user = new User(usual_user_info_val[0], usual_user_info_val[1], InitActivity.getUsername(), usual_user_info_val[3], usual_user_info_val[2], InitActivity.getPassword(), "هیات علمی", "هیات علمی", uniname.getText().toString(), level_values[spinner_values[0]], subject.getText().toString(), goal_values[spinner_values[1]]);
                    try {
                        send_req(user, view);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(view.getContext(), "لطفا همه مقادیر را وارد کنید", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void fill_rest_sazman(LayoutInflater vi, String[] specific_info, String[] user_specific_info_val, LinearLayout holder, final String[] usual_user_info_val) {


        final int[] spinner_values = new int[]{-1, -1, -1};//  [0]=ins name   [1]=proj name [2]=role


        ConstraintLayout ppp = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) ppp.findViewById(R.id.tl_ch4)).setHint(specific_info[1]);
        final AutoCompleteTextView proj_name = ((AutoCompleteTextView) ppp.findViewById(R.id.ed_editprofile_1));
//        proj_name.setText(user_specific_info_val[1]);
        final ArrayList<String> projects_names = new ArrayList<>();

        final ArrayAdapter ad1 = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, projects_names);
        proj_name.setAdapter(ad1);
        proj_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] = i;
            }
        });


        ConstraintLayout inssss = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) inssss.findViewById(R.id.tl_ch4)).setHint(specific_info[0]);
        final AutoCompleteTextView insss_name = ((AutoCompleteTextView) inssss.findViewById(R.id.ed_editprofile_1));
//        insss_name.setText(user_specific_info_val[0]);
        final ArrayList<String> institutions_names = new ArrayList<>();

        ArrayAdapter names_Adapter = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, institutions_names);
        insss_name.setAdapter(names_Adapter);
        insss_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[0] = i;
                spinner_values[1] = -1;


                Map<String, ArrayList<String>> map = GetOrganList.getOrgans();
                projects_names.clear();
                ArrayList<String> sinas_shitty_list = map.get(institutions_names.get(spinner_values[0]));
                for (int j = 1; j < sinas_shitty_list.size(); j++) {
                    projects_names.add(sinas_shitty_list.get(j));

                }
                sazman_id = sinas_shitty_list.get(0);


                ad1.notifyDataSetChanged();
            }
        });
        holder.addView(inssss);
        holder.addView(ppp);


        ConstraintLayout aaa = (ConstraintLayout) vi.inflate(R.layout.child4, null);
        ((TextInputLayout) aaa.findViewById(R.id.tl_ch4)).setHint(specific_info[2]);
        final AutoCompleteTextView user_role = ((AutoCompleteTextView) aaa.findViewById(R.id.ed_editprofile_1));
//        user_role.setText(user_specific_info_val[2]);
        final String[] roles = new String[]{"مسئول سازمان", "مسئول پروژه", "برداشت کننده داده"};

        ArrayAdapter ad2 = new ArrayAdapter(root.getContext(), R.layout.support_simple_spinner_dropdown_item, roles);
        user_role.setAdapter(ad2);
        user_role.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[2] = i;
            }
        });
        holder.addView(aaa);


        projects_names.clear();
        MyUtiles.GetOrgan(getActivity(), names_Adapter, institutions_names, proj_name, user_role, insss_name);


        Button create = root.findViewById(R.id.b_editprofile);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                check_usual_fields();

                if (spinner_values[0] == -1) {
                    ready = false;
                }
                if (spinner_values[1] == -1) {
                    ready = false;
                }
                if (spinner_values[2] == -1) {
                    ready = false;
                }


                if (ready) {
                   // Log.d("apiupdateuserinfo", -1+"---"+usual_user_info_val[0]+"---"+usual_user_info_val[1]+"---"+InitActivity.getUsername()+"---"+usual_user_info_val[3]+"---"+usual_user_info_val[2]+"---"+InitActivity.getPassword()+"---"+Integer.parseInt(sazman_id)+"---"+InitActivity.getProject_start_date()+"---"+projects_names.get(spinner_values[1])+"---"+roles[spinner_values[2]]+"---"+InitActivity.getProject_km());
                    User user = new User(-1, usual_user_info_val[0], usual_user_info_val[1], InitActivity.getUsername(), usual_user_info_val[3], usual_user_info_val[2], InitActivity.getPassword(), "سازمانی", "سازمانی", Integer.parseInt(sazman_id), "", projects_names.get(spinner_values[1]), roles[spinner_values[2]],   "100");
                    try {
                        send_req(user, view);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(view.getContext(), "لطفا همه مقادیر را وارد کنید", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void fill_rest_casual(final String[] usual_user_info_val) {


        Button create = root.findViewById(R.id.b_editprofile);
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                check_usual_fields();

                if (ready) {
                    User user = new User(usual_user_info_val[0], usual_user_info_val[1], InitActivity.getUsername(), usual_user_info_val[3], usual_user_info_val[2], InitActivity.getPassword(), "عادی", InitActivity.getUserJob());
                    try {
                        send_req(user, view);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    private void check_usual_fields() {
        ready = true;
        for (int i = 0; i < usual_user_info_val.length; i++) {
            if (usual_fields[i].getText().toString().isEmpty()) {
                ready = false;
                usual_fields[i].setError("ضروری");
            } else {
                usual_user_info_val[i] = usual_fields[i].getText().toString();

            }
        }

    }
}