package com.ace.roadstateevaluator.fragments.login;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.LoginActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;


public class LoginFragment extends Fragment {


    private View root;
    private LoginActivity activity;
    public static LoginFragment newInstance(LoginActivity activity) {
        return new LoginFragment(activity);
    }

    public LoginFragment(LoginActivity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root=inflater.inflate(R.layout.login_fragment, container, false);

        Log.d("QQQQQQQQQQQQ","login");

        init();

        return root;
    }

    private void init() {
        final TextInputEditText email=root.findViewById(R.id.ed_login_email);
        final TextInputEditText pass=root.findViewById(R.id.ed_login_pass);
        final Button login=root.findViewById(R.id.b_login_login);
        final Button signup=root.findViewById(R.id.b_login_signup);
        TextView forget=root.findViewById(R.id.tv_login_forget);
        TextView rules=root.findViewById(R.id.tv_login_rules);
        final ProgressBar pb=root.findViewById(R.id.pb_login);
        final ImageView imageView = root.findViewById(R.id.imageView);

        final Handler handler = new Handler(Looper.getMainLooper()); //
        Runnable runnable = new Runnable() {
            public void run() {
                imageView.setTransitionName(null);
            }
        };
        handler.postDelayed(runnable, 1500);
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean ready=true;

                if (email.getText().toString().trim().isEmpty()){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) email.getParent().getParent();
                    layout.setError("");
                    Toast.makeText(activity, "لطفا نام کاربری خود را وارد کنید", Toast.LENGTH_SHORT).show();
                }
                if (pass.getText().toString().isEmpty()){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) pass.getParent().getParent();
                    layout.setError("");
                    Toast.makeText(activity, "لطفا رمز ورود خود را وارد کنید", Toast.LENGTH_SHORT).show();
                }



                if (ready) {
                    pb.setVisibility(View.VISIBLE);
                    login.setActivated(false);
                    signup.setActivated(false);


                    try {
                        MyUtiles.GetLogin(email.getText().toString(), pass.getText().toString(),view.getContext(),getActivity(),pass,getActivity(),pb);


                        login.setActivated(true);
                        signup.setActivated(true);


                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(root.getContext(), "خطا در برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();
                        pb.setVisibility(View.INVISIBLE);
                        login.setActivated(true);
                        signup.setActivated(true);

                    }
                }
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.go_signup();
            }
        });

        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



    }


}