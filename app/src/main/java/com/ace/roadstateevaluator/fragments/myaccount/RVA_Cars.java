package com.ace.roadstateevaluator.fragments.myaccount;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.AssetManage;
import com.ace.roadstateevaluator.DB.CarDB;
import com.ace.roadstateevaluator.GsonModels.Car;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;

import java.util.ArrayList;

public class RVA_Cars extends RecyclerView.Adapter<RVA_Cars.VH> {

    AssetManage assetManage;
    private ArrayList<Car> all;


    public RVA_Cars(ArrayList<Car> all) {
        this.all = all;
    }


    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;

        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.eachcar, parent, false);


        return new VH(v);
    }


    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {

        return all.size() + 1;
    }

    public class VH extends RecyclerView.ViewHolder {

        //make null objs
        TextView name, year, km, nameh, yearh, kmh,take_km,take_kmh;
        View ll;


        public VH(@NonNull final View itemView) {
            super(itemView);
            assetManage = new AssetManage();

            ll = itemView.findViewById(R.id.ll_eachcar);

            name = itemView.findViewById(R.id.tv_eachcar_name);
            nameh = itemView.findViewById(R.id.tv_eachcar_nameh);

            year = itemView.findViewById(R.id.tv_eachcar_year);
            yearh = itemView.findViewById(R.id.tv_eachcar_yearh);


            km = itemView.findViewById(R.id.tv_eachcar_km);
            kmh = itemView.findViewById(R.id.tv_eachcar_kmh);


            take_km = itemView.findViewById(R.id.tv_eachcartake_km);
            take_kmh = itemView.findViewById(R.id.tv_eachcartake_kmh);


        }

        public void bind(final int i) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (i != all.size()) {
                        //normal onclick

                        Car c = all.get(i);
                        if (!c.getSelected()) {
                            CarDB db = new CarDB(view.getContext());
                            for (Car car : all) {
                                if (car.getSelected()) {
                                    car.setSelected(false);
                                    db.UpdateDB(car);
                                }
                            }
                            c.setSelected(true);
                            InitActivity.setCarId(c.getCarid());
                            db.UpdateDB(c);
                            notifyDataSetChanged();
                        }
                    } else {
                        //add new onclick
                        new_car(itemView.getContext());

                    }

                }
            });


            if (i != all.size()) {
                itemView.findViewById(R.id.ll_eachcar).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.cl_newcar).setVisibility(View.INVISIBLE);
                Car c = all.get(i);
                name.setText(c.getCarname());
                year.setText(c.getCreatyear() + "");
                km.setText(c.getUsagekm() + "");
                take_km.setText(c.getCarlength()+"");


                if (c.getSelected()) {

                    ll.setBackground(itemView.getContext().getDrawable(R.drawable.back_each_carblue));
                    name.setTextColor(Color.WHITE);
                    year.setTextColor(Color.WHITE);
                    km.setTextColor(Color.WHITE);
                    take_km.setTextColor(Color.WHITE);

                    nameh.setTextColor(itemView.getContext().getColor(R.color.textcaswhite));
                    yearh.setTextColor(itemView.getContext().getColor(R.color.textcaswhite));
                    kmh.setTextColor(itemView.getContext().getColor(R.color.textcaswhite));
                    take_kmh.setTextColor(itemView.getContext().getColor(R.color.textcaswhite));

                } else {

                    ll.setBackground(itemView.getContext().getDrawable(R.drawable.back_each_car));
                    name.setTextColor(Color.BLACK);
                    year.setTextColor(Color.BLACK);
                    km.setTextColor(Color.BLACK);
                    take_km.setTextColor(Color.BLACK);

                    nameh.setTextColor(itemView.getContext().getColor(R.color.casualtv));
                    yearh.setTextColor(itemView.getContext().getColor(R.color.casualtv));
                    kmh.setTextColor(itemView.getContext().getColor(R.color.casualtv));
                    take_kmh.setTextColor(itemView.getContext().getColor(R.color.casualtv));

                }


            }else {

                itemView.findViewById(R.id.ll_eachcar).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.cl_newcar).setVisibility(View.VISIBLE);
            }



        }
    }

    private void new_car(Context context) {


        if (MyUtiles.isNetworkConnected(context)) {

            MainActivity.mainActivity.navController.navigate(R.id.action_navigation_notifications_to_newcarFragment);
        } else {
            Toast.makeText(context, "خطا در برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();
        }


    }


}
