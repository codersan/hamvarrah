package com.ace.roadstateevaluator.fragments.Alley;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.transition.TransitionInflater;

import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.R;

public class AboutusFragment extends Fragment implements View.OnClickListener {

    private View root;


    public static AboutusFragment newInstance() {
        return new AboutusFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        root=inflater.inflate(R.layout.aboutus_fragment, container, false);

        init();

        return root;
    }

    private void init() {
        ImageView web,email,linkedin,telegram;

//        Button b=root.findViewById(R.id.b_aboutus);
//        b.setOnClickListener(this);

        TextView ace=root.findViewById(R.id.aboutace);
        ace.setOnClickListener(this);


        web=root.findViewById(R.id.iv_web);
        email=root.findViewById(R.id.iv_email);
        linkedin=root.findViewById(R.id.iv_linkedin);
        telegram=root.findViewById(R.id.iv_tel);


        web.setOnClickListener(this);
        email.setOnClickListener(this);
        linkedin.setOnClickListener(this);
        telegram.setOnClickListener(this);








    }


    @Override
    public void onClick(View view) {
        int type= Integer.valueOf(view.getTag().toString());

        switch (type){
            case 0:
                String url = "https://t.me/hamvar_rah";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

                break;
            case 1:
                String url1 = "http://www.hamvar-rah.ir";
                Intent i1 = new Intent(Intent.ACTION_VIEW);
                i1.setData(Uri.parse(url1));
                startActivity(i1);
                break;
            case 2:
                String url0 = "https://www.linkedin.com/company/hamvar-rah/";
                Intent i0 = new Intent(Intent.ACTION_VIEW);
                i0.setData(Uri.parse(url0));
                startActivity(i0);
                break;
            case 3:
                //email
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","hamvar.rah99@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "هموار راه");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "ارسال ایمیل"));
                break;
            case 4:
                String urlace = "http://www.ace-team.ir";
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                i2.setData(Uri.parse(urlace));
                startActivity(i2);
                break;



        }

    }

    @Override
    public void onResume() {
        super.onResume();

        MainActivity.mainActivity.should_this_shit_be_back_button=true;
    }
}