package com.ace.roadstateevaluator.fragments.Alley;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.Vars;

public class ViewProfileFragment extends Fragment {

    private View root;


    public static ViewProfileFragment newInstance() {
        return new ViewProfileFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        MainActivity.mainActivity.should_this_shit_be_back_button=true;

        root = inflater.inflate(R.layout.viewprofile_fragment, container, false);

        fill_overall_details();
        init();

        return root;
    }

    private void init() {
        Button exit = root.findViewById(R.id.b_viewprof_exit);
        Button edit = root.findViewById(R.id.b_viewprof_edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.mainActivity.navController.navigate(R.id.action_viewProfileFragment_to_editProfileFragment);
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Dialog dialog = new Dialog(view.getContext(),R.style.exitDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                LayoutInflater inflater = LayoutInflater.from(view.getContext());
                final View mainview = inflater.inflate(R.layout.exit_dialog, null);
                dialog.setContentView(mainview);




                TextView title=mainview.findViewById(R.id.textView);
                TextView message=mainview.findViewById(R.id.tv_exit_message);

                title.setText("خروج از حساب کاربری");
                message.setText("آیا می خواهید از حساب کاربری خود خارج شوید؟");



                Button full_exit = mainview.findViewById(R.id.b_exit_full);
                Button background_exit = mainview.findViewById(R.id.b_exit_back);

                full_exit.setText("بله خروج از حساب");
                background_exit.setText("خیر");

                full_exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        MyUtiles.SignOut(view.getContext());


                    }
                });
                background_exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();


                    }
                });


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = (int) (Vars.width * 0.9);
                lp.height = (int) (Vars.height * 0.5);
                dialog.getWindow().setAttributes(lp);
                dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.backdialoge));





                dialog.show();


            }
        });
    }


    private void fill_overall_details() {


        String[] usual_user_info_val = new String[]{InitActivity.getUserFirstName(), InitActivity.getUserFamilyName(), InitActivity.getUsername(), InitActivity.getUserPhone(), InitActivity.getUserEmail(), InitActivity.getUserTypePersian()};


        //get the mother view
        LinearLayout holder = root.findViewById(R.id.ll_viewprofile);
        //get info text array
        String[] usual_info = root.getContext().getResources().getStringArray(R.array.userinfo_all);
        //get an Inflater
        LayoutInflater vi = (LayoutInflater) root.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //foreach text in the array add a view to the mother
        for (int i = 0; i < usual_info.length; i++) {


            ConstraintLayout child = (ConstraintLayout) vi.inflate(R.layout.child2, null);
            ((TextView) child.findViewById(R.id.tt_child2_key)).setText(usual_info[i]);
            ((TextView) child.findViewById(R.id.tt_child2_val)).setText(usual_user_info_val[i]);


            holder.addView(child);


        }
        //get description text array
        String[] specific_info = new String[0];
        String[] user_specific_info_val = new String[0];

        //fill the rest based on USER TYPE
        int type = InitActivity.UserTypeIndex;

        switch (type) {
            case 0:
                specific_info = new String[]{"شغل"};
                user_specific_info_val = new String[]{InitActivity.getUserJob()};
                break;
            case 1:
                specific_info = root.getContext().getResources().getStringArray(R.array.userinfo_student);
                user_specific_info_val = new String[]{InitActivity.getStudent_university(), MyUtiles.EnglishToPersian.get(InitActivity.getStudent_level()), InitActivity.getStudent_supervisor(), InitActivity.getStudent_topic(), MyUtiles.EnglishToPersian.get(InitActivity.getStudent_target())};
                break;
            case 2:
                specific_info = root.getContext().getResources().getStringArray(R.array.userinfo_heiat);
                user_specific_info_val = new String[]{InitActivity.getProfessor_university(), MyUtiles.EnglishToPersian.get(InitActivity.getProfessor_degree()), InitActivity.getProfessor_topic(), MyUtiles.EnglishToPersian.get(InitActivity.getProfessor_target())};
                break;
            case 3:

                specific_info = root.getContext().getResources().getStringArray(R.array.userinfo_sazmani);
                user_specific_info_val = new String[]{InitActivity.getProject_organizations_id() + "", InitActivity.getProject_name(), MyUtiles.EnglishToPersian.get(InitActivity.getProject_user_role())};

                break;


        }
        for (int i = 0; i < specific_info.length; i++) {


            ConstraintLayout child = (ConstraintLayout) vi.inflate(R.layout.child2, null);
            ((TextView) child.findViewById(R.id.tt_child2_key)).setText(specific_info[i]);
            ((TextView) child.findViewById(R.id.tt_child2_val)).setText(user_specific_info_val[i]);


            holder.addView(child);


        }

    }


}