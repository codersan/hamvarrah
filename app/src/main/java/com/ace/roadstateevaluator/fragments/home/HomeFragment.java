package com.ace.roadstateevaluator.fragments.home;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.CollectData;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.Vars;
import com.google.android.material.snackbar.Snackbar;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;

import org.json.JSONException;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.gridlines.LatLonGridlineOverlay2;
import org.osmdroid.views.overlay.mylocation.DirectedLocationOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import pl.pawelkleczkowski.customgauge.CustomGauge;


public class HomeFragment extends Fragment {
    MapView map = null;
    private MapView osm;
    private MapController mc;
    private LocationManager locationManager;
    LocationListener listener;
    private TextView speedtv;
    private CustomGauge gauge;
    ArrayList<OverlayItem> overlayItemArray;
    private static final int PERMISSAO_REQUERIDA = 1;
    public static HomeFragment homeFragment = null;
    View root;
    private boolean open = false;
    TextView dis, valid_dis, top_sumdis, top_sumdisvalid, top_car, top_freq, sazmani_sum, sazmani_validsum;
    public Button start;
    private Runnable runnable;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_home, container, false);
        init();
        FillUiValue();
        Context ctx = root.getContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        MyLocationNewOverlay myLocationNewOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(root.getContext()), osm);
        myLocationNewOverlay.enableMyLocation();
        osm.getOverlays().add(myLocationNewOverlay);
        GeoPoint startPoint = new GeoPoint(35.6892, 51.3890);
        mc.setCenter(startPoint);
        //addMarker(center);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 50, 0, listener);
        Log.d("Sloccccc", "updated");
        return root;
    }

    @Override
    public void onPause() {
        homeFragment = null;
        super.onPause();
    }

    @Override
    public void onStop() {
        homeFragment = null; //for setting values and names in homefrag uses if is available
        super.onStop();
    }

    private void updateLoc(Location loc) {
        GeoPoint locGeoPoint = new GeoPoint(loc.getLatitude(), loc.getLongitude());
        mc.setCenter(locGeoPoint);
        mc.setZoom(19);
        osm.invalidate();
    }


    private void init_animation() {

        final ImageView more = root.findViewById(R.id.v_main_openinfo);
        final View cl = root.findViewById(R.id.cl_main_top);


        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("QQQQQQQQQQQQ", "click");


                if (open) {
                    more.setImageResource(R.drawable.ic_baseline_more_horiz_24);

                    Animator animator = ViewAnimationUtils.createCircularReveal(cl, view.getWidth() / 2, view.getHeight() / 2, Vars.width, 0);
                    animator.setDuration(500);
                    animator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            cl.setVisibility(View.INVISIBLE);
                        }
                    });
//                    animator.addListener(new AnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            super.onAnimationEnd(animation);
//
//
//                        }
//                    });
                    animator.start();
                    open = false;
                } else {
                    more.setImageResource(R.drawable.ic_baseline_close_24);
                    Animator animator = ViewAnimationUtils.createCircularReveal(cl, view.getWidth() / 2, view.getHeight() / 2, 0, Vars.width);
                    animator.setDuration(500);
//                    animator.addListener(new AnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            super.onAnimationEnd(animation);
//                        }
//                    });
                    cl.setVisibility(View.VISIBLE);
                    animator.start();
                    open = true;
                }
            }
        });
    }

    private void init() {
        //---------------------- for defult top value in takefreagment
        try {
            MyUtiles.GetFreqSpeed(root.getContext());
            MyUtiles.GetTotalDis("0",root.getContext());
            MyUtiles.GetTotalDis("1",root.getContext());
            MyUtiles.GetSpeeds("avg",root.getContext());
            MyUtiles.GetSpeeds("max",root.getContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //------------------------
        homeFragment = this;
        start = root.findViewById(R.id.b_home);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CollectData.GetInstance().ChangeState(1,v.getContext());
            }
        });
        dis = root.findViewById(R.id.tv_home_dis);
        valid_dis = root.findViewById(R.id.tv_home_validdis);

        top_car = root.findViewById(R.id.tv_home_car);
        top_sumdis = root.findViewById(R.id.tv_home_topdis);
        top_sumdisvalid = root.findViewById(R.id.tv_home_validtopdis);
        top_freq = root.findViewById(R.id.tv_home_freq);
        top_freq.setText(InitActivity.getFreq() + "");
        sazmani_sum = root.findViewById(R.id.tv_home_sazmani_sum);
        sazmani_validsum = root.findViewById(R.id.tv_home_sazmani_validsum);
        if (true) {
//            if (InitActivity.UserTypeIndex!=3){
            sazmani_sum.setVisibility(View.GONE);
            sazmani_validsum.setVisibility(View.GONE);
            root.findViewById(R.id.holder19).setVisibility(View.GONE);
            root.findViewById(R.id.holder18).setVisibility(View.GONE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(root.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] permissoes = {Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION};
                requestPermissions(permissoes, PERMISSAO_REQUERIDA);
            }
        }
        osm = (MapView) root.findViewById(R.id.mapaId);
        osm.setTileSource(TileSourceFactory.MAPNIK);
        osm.setBuiltInZoomControls(false);
        osm.setMultiTouchControls(true);
        mc = (MapController) osm.getController();
        mc.setZoom(19);
        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                //Log.d("Sloccccc1",location.getLatitude()+"---"+location.getLongitude());
                updateLoc(location);

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        init_animation();
        gauge = root.findViewById(R.id.gauge1);
        speedtv = root.findViewById(R.id.tv_home_speed);
        change_speed(70);
    }

    private void change_speed(int speed) {
        if(speed<=25){
            gauge.setPointStartColor(getActivity().getResources().getColor(R.color.red));
            gauge.setPointEndColor(getActivity().getResources().getColor(R.color.red));
        }else {
            gauge.setPointStartColor(getActivity().getResources().getColor(R.color.colorPrimary));
            gauge.setPointEndColor(getActivity().getResources().getColor(R.color.colorPrimary));
        }
        gauge.setValue(speed);
        speedtv.setText(speed + "");


    }

    public void FillUiValue() {

        change_speed((int) InitActivity.getCurentspeed());
        dis.setText(String.format("%.01f", InitActivity.getCurenttotallength()));
        valid_dis.setText(String.format("%.01f",InitActivity.getCurentvalidtotallength() ));
        top_sumdis.setText(String.format("%.01f",InitActivity.getTotallength()/1000 ));
        top_sumdisvalid.setText(String.format("%.01f",InitActivity.getValidtotallength()/1000 ));
        if (GetUserCarsList.getNameAndId().containsKey(InitActivity.getCarId())) {
            top_car.setText(GetUserCarsList.getNameAndId().get(InitActivity.getCarId()));
        }
        top_freq.setText(InitActivity.getFreq() + "");
        if (CollectData.GetInstance().Startcheck) {
            start.setBackgroundColor(getActivity().getResources().getColor(R.color.red));
            start.setText("توقف");
        } else {
            start.setBackgroundColor(getActivity().getResources().getColor(R.color.green));
            start.setText("شروع");
        }
    }

    @Override
    public void onResume() {
        homeFragment=this;
        super.onResume();
        FillUiValue();
        MainActivity.mainActivity.should_this_shit_be_back_button=false;


    }
}