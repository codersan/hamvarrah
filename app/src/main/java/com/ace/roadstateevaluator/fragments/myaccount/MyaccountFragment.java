package com.ace.roadstateevaluator.fragments.myaccount;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.CollectData;
import com.ace.roadstateevaluator.DB.CarDB;
import com.ace.roadstateevaluator.DB.TakeDB;
import com.ace.roadstateevaluator.GpsService;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.RetrofitResponseListener;
import com.ace.roadstateevaluator.Vars;

import org.json.JSONException;

public class MyaccountFragment extends Fragment {

    private View root;
    private TextView freq;
    public static RVA_Cars rva_cars;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_myaccount, container, false);

        try {
            init(root.getContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return root;
    }

    private void init(Context c) throws JSONException {
        //------------------
        TextView name, username, email, number, type;
        TextView exit = root.findViewById(R.id.b_myaccount_exit);
        TextView more = root.findViewById(R.id.b_myaccount_more);
        TextView edit = root.findViewById(R.id.b_myaccount_edit);
        name = root.findViewById(R.id.tv_myaccount_name);
        username = root.findViewById(R.id.tv_myaccount_username);
        email = root.findViewById(R.id.tv_myaccount_email);
        number = root.findViewById(R.id.tv_myaccount_number);
        type = root.findViewById(R.id.tv_myaccount_type);
        freq = root.findViewById(R.id.tv_myaccount_frq);


        name.setText(InitActivity.getUserFirstName() + " " + InitActivity.getUserFamilyName());
        username.setText(InitActivity.getUsername());
        email.setText(InitActivity.getUserEmail());
        number.setText(InitActivity.getUserPhone());
        type.setText(InitActivity.getUserTypePersian());
        freq.setText(InitActivity.getFreq() + "");


        RecyclerView rv = root.findViewById(R.id.rv_myaccount_cars);
        rva_cars = new RVA_Cars(GetUserCarsList.getCars());
        rv.setAdapter(rva_cars);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setLayoutManager(new LinearLayoutManager(root.getContext(), RecyclerView.HORIZONTAL, false));
        //------------------------
        if (GetUserCarsList.getCars().size() == 0 ||GetUserCarsList.getCars()==null) { //check if car is empty get from server list
            MyUtiles.GetCars(c, new RetrofitResponseListener() {
                @Override
                public void onSuccess() {
                    rva_cars.notifyDataSetChanged();
                }

                @Override
                public void onFailure() {

                }
            });
        }

        MyUtiles.GetTotalDisPerCar(c);//update each time come to freagment

        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.mainActivity.navController.navigate(R.id.action_navigation_notifications_to_viewProfileFragment);
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //show exit dialog

                final Dialog dialog = new Dialog(view.getContext(), R.style.exitDialog);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                LayoutInflater inflater = LayoutInflater.from(view.getContext());
                final View mainview = inflater.inflate(R.layout.exit_dialog, null);
                dialog.setContentView(mainview);


                TextView title = mainview.findViewById(R.id.textView);
                TextView message = mainview.findViewById(R.id.tv_exit_message);

                title.setText("خروج از حساب کاربری");
                message.setText("آیا می خواهید از حساب کاربری خود خارج شوید؟");


                Button full_exit = mainview.findViewById(R.id.b_exit_full);
                Button background_exit = mainview.findViewById(R.id.b_exit_back);

                full_exit.setText("بله خروج از حساب");
                background_exit.setText("خیر");

                full_exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MyUtiles.SignOut(view.getContext());


                    }
                });
                background_exit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();


                    }
                });


                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = (int) (Vars.width * 0.9);
                lp.height = (int) (Vars.height * 0.5);
                dialog.getWindow().setAttributes(lp);
                dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.backdialoge));


                dialog.show();

            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show_freq_dialog(view.getContext());
            }
        });




    }

    private void show_freq_dialog(Context context) {
        //create new car
        final Dialog dialog = new Dialog(context, R.style.PauseDialog);
        LayoutInflater inflater = LayoutInflater.from(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View mainview = inflater.inflate(R.layout.edit_freq, null);
        dialog.setContentView(mainview);


        Button b = mainview.findViewById(R.id.b_edittake_b);
        final EditText ed = mainview.findViewById(R.id.ed_edittake_des);
        ed.setText(InitActivity.getFreq() + "");
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed.getText().toString().isEmpty()) {
                    ed.setError("فرکانس نامعنبر");
                } else {

                    InitActivity.setFreq(Integer.valueOf(ed.getText().toString()));

                    freq.setText(InitActivity.getFreq() + "");

                    dialog.dismiss();
                }
            }
        });


        //make dialog BIG
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (Vars.width * 1);
        lp.height = (int) (Vars.height * 0.5);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.backdialoge));


        //
        Window window = dialog.getWindow();
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);


        dialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (rva_cars!=null) {
            rva_cars.notifyDataSetChanged();
        }
        MainActivity.mainActivity.should_this_shit_be_back_button=true;

    }
}