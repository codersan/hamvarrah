package com.ace.roadstateevaluator.fragments.takes;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.GsonModels.GetTakeList;
import com.ace.roadstateevaluator.GsonModels.TakeSession;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.RetrofitResponseListener;

import org.json.JSONException;

import java.util.ArrayList;

public class TakesFragment extends Fragment {

    private View root;
    RVA_Takes rva_takes;
    RecyclerView rv;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_takes, container, false);

        init();
        try {
            get_Takes_from_net_and_db();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return root;
    }

    private void get_Takes_from_net_and_db() throws JSONException {


        if (MyUtiles.isNetworkConnected(root.getContext())){
            MyUtiles.GetTakes(root.getContext(), new RetrofitResponseListener() {
                @Override
                public void onSuccess() {
                   //MyUtiles.ReadTakeDb(root.getContext()); its done when set array when get


                    check_if_empty();
//                    rva_takes=new RVA_Takes(GetTakeList.getTakes());
//                    rv.setAdapter(rva_takes);
                   // rva_takes.notifyDataSetChanged();

                    Log.d("apisinaaaad","notify change!! "+GetTakeList.getTakes().size()+" "+rva_takes.getItemCount());
                }

                @Override
                public void onFailure() {

                }
            });
            //-----------------update the top values
            MyUtiles.GetFreqSpeed(root.getContext());
            MyUtiles.GetTotalDis("0",root.getContext());
            MyUtiles.GetTotalDis("1",root.getContext());
            MyUtiles.GetSpeeds("avg",root.getContext());
            MyUtiles.GetSpeeds("max",root.getContext());
        }



    }

    private void init() {

        fill_overall_details();
        MyUtiles.ReadTakeDb(root.getContext());
        rv=root.findViewById(R.id.rv_takes_takes);
         rva_takes=new RVA_Takes(GetTakeList.getTakes());
        rv.setAdapter(rva_takes);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setLayoutManager(new LinearLayoutManager(root.getContext()));





        check_if_empty();


    }

    private void check_if_empty(){
        if (GetTakeList.getTakes().size()==0){
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) rv.getLayoutParams();
            params.height = 0;
            rv.setLayoutParams(params);

            root.findViewById(R.id.no_takse).setVisibility(View.VISIBLE);

        }else {
            root.findViewById(R.id.no_takse).setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity.mainActivity.should_this_shit_be_back_button=true;

    }

    private void fill_overall_details(){
        ArrayList<String> values = new ArrayList<>();
        values.add(String.format("%.01f", InitActivity.getTotallength()/1000)); //show in km
        values.add(String.format("%.01f", InitActivity.getValidtotallength()/1000));
        values.add(String.format("%.01f", InitActivity.getMaxspeed()));
        values.add(String.format("%.01f", InitActivity.getAvgspeed()));
        values.add(InitActivity.getFreqspeed()+"");
        values.add(String.format("%.01f", InitActivity.getCurentmaxspeed()));
        values.add(String.format("%.01f", InitActivity.getCurentavgspeed()));
        //get the mother view
        LinearLayout holder=root.findViewById(R.id.ll_bardasht_detailsholder);
        //get description text array
        String[] descriptions = root.getContext().getResources().getStringArray(R.array.overalldetails);
        //get an Inflater
        LayoutInflater vi = (LayoutInflater) root.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //foreach text in the array add a view to the mother
        for (int i=0;i<descriptions.length;i++) {


            LinearLayout child = (LinearLayout) vi.inflate(R.layout.child1, null);

            //description
            ((TextView)child.findViewById(R.id.tt_child_des)).setText(descriptions[i]);

            //value
            ((TextView)child.findViewById(R.id.tt_child_val)).setText(values.get(i));







            holder.addView(child);


        }
    }
}