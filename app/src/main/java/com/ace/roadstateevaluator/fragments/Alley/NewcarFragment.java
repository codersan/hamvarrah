package com.ace.roadstateevaluator.fragments.Alley;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.DB.CarDB;
import com.ace.roadstateevaluator.GsonModels.Car;
import com.ace.roadstateevaluator.GsonModels.CarModels;
import com.ace.roadstateevaluator.GsonModels.GetServerCarsModel;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.RetrofitResponseListener;
import com.ace.roadstateevaluator.fragments.myaccount.MyaccountFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class NewcarFragment extends Fragment {

    private View root;
    private ArrayList<CarModels> models;
    private ArrayList<String> brands;

    private ArrayList<String> models_names;

    private ArrayAdapter<String> model_adapter;
    private ArrayAdapter<String> brand_adapter;


    public static NewcarFragment newInstance() {
        return new NewcarFragment();
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        MainActivity.mainActivity.should_this_shit_be_back_button=true;

        root=inflater.inflate(R.layout.newcar_fragment, container, false);


        init(root.getContext());

        return root;
    }

    private void init(Context c) {
        final int[] spinner_values=new int[]{-1,-1};

        final TextInputEditText year=root.findViewById(R.id.ed_newcar_year);
        final TextInputEditText km=root.findViewById(R.id.ed_newcar_km);
        final TextInputEditText customname=root.findViewById(R.id.ed_newcar_customname);
        final AutoCompleteTextView brand=root.findViewById(R.id.sp_newcar_brand);
        final AutoCompleteTextView car=root.findViewById(R.id.sp_newcar_car);


        brands= new ArrayList<>();
        brand_adapter =new ArrayAdapter<String>(root.getContext(),R.layout.support_simple_spinner_dropdown_item,brands);



        MyUtiles.GetCarModels(c,brand_adapter,brands,brand);
        final HashMap<String, ArrayList<CarModels>> map=GetServerCarsModel.getCars();





//        for (Object b:map.keySet().toArray()) {
//            brands.add(b.toString());
//
//        }
        models= new ArrayList<>();
        models_names=new ArrayList<>();




        brand.setAdapter(brand_adapter);




        model_adapter =new ArrayAdapter<String>(root.getContext(),R.layout.support_simple_spinner_dropdown_item,models_names);
        car.setAdapter(model_adapter);

        brand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                car.setEnabled(true);

                if (spinner_values[0]==i){

                }else {
                    Log.d("!!!!!!!!!!!!!!!!!!!!",i+" : "+map.get(brands.get(i)).size());

                    spinner_values[0] =i;


                    models=map.get(brands.get(i));
                    models_names.clear();
                    for (CarModels m:models) {
                        models_names.add(m.getModel());
                    }


                    model_adapter.notifyDataSetChanged();
                    spinner_values[1] =-1;

                }



            }
        });


        car.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                spinner_values[1] =i;
            }
        });







        Button b=root.findViewById(R.id.b_newcar);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean ready=true;

                if (year.getText().toString().isEmpty()){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) year.getParent().getParent();
                    layout.setError("ضروری");
                }
                if (km.getText().toString().isEmpty()){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) km.getParent().getParent();
                    layout.setError("ضروری");
                }
                if (customname.getText().toString().isEmpty()){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) customname.getParent().getParent();
                    layout.setError("ضروری");
                }
                if (spinner_values[0]==-1){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) brand.getParent().getParent();
                    layout.setError("ضروری");
                }

                if (spinner_values[1]==-1){
                    ready=false;
                    final TextInputLayout layout= (TextInputLayout) car.getParent().getParent();
                    layout.setError("ضروری");
                }
                if (ready){
                   final Car newcar=new Car(map.get(brands.get(spinner_values[0])).get(spinner_values[1]).getId(),Integer.valueOf(year.getText().toString()),Integer.valueOf(km.getText().toString()), InitActivity.getUserId(),customname.getText().toString(),false);
                    try {
                        final  View viewfinal = view;
                        MyUtiles.AddCar(newcar, view.getContext(), new RetrofitResponseListener() {
                            @Override
                            public void onSuccess() {

                                try {
                                    MyUtiles.GetCars(viewfinal.getContext(), new RetrofitResponseListener() {
                                        @Override
                                        public void onSuccess() {
                                            MyaccountFragment.rva_cars.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onFailure() {

                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                MainActivity.mainActivity.navController.popBackStack();
                            }

                            @Override
                            public void onFailure() {

                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //sina






                }else {
                    Snackbar.make(view,"لطفا مقادیر مورد نیاز را وارد نمایید",Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }


}