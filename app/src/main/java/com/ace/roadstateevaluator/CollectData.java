package com.ace.roadstateevaluator;

import android.app.Dialog;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ace.roadstateevaluator.Activity.InitActivity;
import com.ace.roadstateevaluator.Activity.MainActivity;
import com.ace.roadstateevaluator.GsonModels.CompletePacket;
import com.ace.roadstateevaluator.GsonModels.DataPacket;
import com.ace.roadstateevaluator.GsonModels.TakeSession;
import com.ace.roadstateevaluator.fragments.home.HomeFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CollectData {
    private Sensor sensor_G, sensor_A;
    private SensorEventListener gravity, acc;
    private SensorManager sensorManager;
    private boolean firstlocation = true;
    public static int Timerstartvalue = 6; // the time we need to delay before start get first signal
    //private static int Timerbetweenvalue = 5; // the maximum time between two signal
    public static boolean Startcheck = false; // must be true to start capture
    private static boolean CheckValid = false;// to check if the insert data is valid or corrupt!
    private TakeSession takeSession;
    private DataPacket dataPacket;
    private CompletePacket completePacket;
    private static CollectData thisclass = null; // to return the same instance of this controller-->(collectdata)
    private Location l1, l2;
    //values for takessesion
    private static float AllMaxSpeed = 0f;
    private static float AllAverageSpeed = 0f;
    private static float AllLength = 0f;
    private static float AllValidLength = 0f;
    private static String StartTime;
    private Timer ts;
    //private Timer tb;
    private TimerTask tsk;
   // private TimerTask tbk;
    private boolean tsstate = false;
    //private boolean tbstate = false;
    //!values for takessesion
    private float Gravity[] = new float[]{1, 1, 1};
    private float Acceleretor[] = new float[]{1, 1, 1};
    private ArrayList<Double> RmsValues = new ArrayList<>();
    static boolean lightwork=true;
    static {
        if (thisclass == null) {
            thisclass = new CollectData();
        }
    }

    //-------------------------------------------------
    private CollectData() {

    }

    //----------------------------------------------------
    //to get one instance of this class to manage all

    public void show_dialog(Context context) {
        //create new car
        final Dialog dialog = new Dialog(context, R.style.PauseDialog);
        LayoutInflater inflater = LayoutInflater.from(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View mainview = inflater.inflate(R.layout.edit_take, null);
        dialog.setContentView(mainview);


        Button b = mainview.findViewById(R.id.b_edittake_b);
        final EditText ed = mainview.findViewById(R.id.ed_edittake_des);
        Log.d("insertsina1", AllLength+"--"+AllMaxSpeed+"--"+AllAverageSpeed / completePacket.getGpsdata().size());

        final CompletePacket completePacket2 = completePacket;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeSession.setDescription(ed.getText().toString()+"");
                takeSession.setTotallen(AllLength); //to metr
                takeSession.setMaxvelocity(AllMaxSpeed);
                takeSession.setMeanvelocity(AllAverageSpeed / completePacket2.getGpsdata().size());
                Log.d("insertsina2", AllLength+"--"+AllMaxSpeed+"--"+AllAverageSpeed / completePacket2.getGpsdata().size());
                takeSession.setCarid(InitActivity.getCarId());
                takeSession.setUserid(InitActivity.getUserId());
                takeSession.setStarttime(StartTime);
                completePacket2.setInfo(takeSession);
            /*Gson gson = new Gson();
            Log.d("insertsina22",gson.toJson(completePacket));*/
                MyUtiles.AllEnd(MainActivity.mainActivity, completePacket2);
                Log.d("insertsina", completePacket2.toString());
                Toast.makeText(MainActivity.mainActivity, "با تشکر از همکاری شما", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);


        //make dialog BIG
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (Vars.width);
        lp.height = (int) (Vars.height * 0.5);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(context.getResources().getDrawable(R.drawable.backdialoge));


        Window window = dialog.getWindow();
        lp.gravity = android.view.Gravity.BOTTOM;
        window.setAttributes(lp);

        dialog.show();

    }

    public static CollectData GetInstance() {
        return thisclass;
    }

    public void Start(int mode,Context c) {// use to start capture with
        try {


            if (InitActivity.getCarId() != 0) {
                LocationManager locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    init();
                    TimerStart();
                    StartTime = MyUtiles.GetDate() + " " + MyUtiles.GetTime();
                    Startcheck = true;
                    takeSession = new TakeSession();
                    dataPacket = new DataPacket();
                    AllValidLength = 0;
                    AllLength = 0;
                    AllAverageSpeed = 0;
                    AllMaxSpeed = 0;
                    completePacket = new CompletePacket();
                    RmsValues = new ArrayList<>();
                    GpsService.GetInstance().StartCapture();
                    MyUtiles.createNotif(c, "در حال برداشت داده...", "توقف داده برداری");
                    if (HomeFragment.homeFragment != null) { //to check if homefreagment is open!
                        //HomeFragment.homeFragment.FillUiValue();//for change btn start and other!
                    }
                } else {
                    Startcheck = false;//set as defult for statechange!
                    Toast.makeText(c, "جی پی اس فعال نمی باشد", Toast.LENGTH_SHORT).show();
                }
            } else {
                Startcheck = false;//set as defult for statechange!
                Toast.makeText(c, "خودرو انتخاب نشده است!", Toast.LENGTH_SHORT).show();

            }
        }catch (Exception e){

        }
    }

    public void Stop(int mode) {// use to stop capture with
        try {
            if (tsk != null) {
                tsk.cancel();
            }
            Log.d("insertsina", "stop1");
            if (CheckValid) {
                Startcheck = false;
                Log.d("insertsina", "stop2");
                if (mode == 1) {
                    show_dialog(MainActivity.mainActivity);
                } else {
                    takeSession.setDescription("");
                    takeSession.setTotallen(AllLength); //to metr
                    takeSession.setMaxvelocity(AllMaxSpeed);
                    takeSession.setMeanvelocity(AllAverageSpeed / completePacket.getGpsdata().size());
                    Log.d("insertsina2", AllLength + "--" + AllMaxSpeed + "--" + AllAverageSpeed / completePacket.getGpsdata().size());
                    takeSession.setCarid(InitActivity.getCarId());
                    takeSession.setUserid(InitActivity.getUserId());
                    takeSession.setStarttime(StartTime);
                    completePacket.setInfo(takeSession);
            /*Gson gson = new Gson();
            Log.d("insertsina22",gson.toJson(completePacket));*/
                    MyUtiles.AllEnd(MainActivity.mainActivity, completePacket);
                    Log.d("insertsina", completePacket.toString());
                    Toast.makeText(MainActivity.mainActivity, "با تشکر از همکاری شما", Toast.LENGTH_SHORT).show();
                }
            }
            MyUtiles.ListFilesAndPost(MainActivity.mainActivity);
            completePacket = new CompletePacket();
            takeSession = new TakeSession();

            GpsService.remove();
            CheckValid = false;
            MyUtiles.createNotif(MainActivity.mainActivity, "توقف برداشت داده!!!", "شروع داده برداری");
            if (HomeFragment.homeFragment != null) { //to check if homefreagment is open!
                HomeFragment.homeFragment.FillUiValue();//for change btn start and other!
            }
        }catch (Exception e){

        }
    }

    public boolean ChangeState(int mod,Context c) { //return the current startcheck after change the state! true for is working! 0for reciver and 1 for app
        if (Startcheck) {
            Startcheck = false;
            Stop(mod);
            Log.d("insertsina", "change 1");
        } else {
            Startcheck = true;
            Start(mod,c);
            Log.d("insertsina", "change 2");
        }
        return Startcheck;
    }

    private void init() {//register listener for sensors at first
        InitActivity.setCurentspeed(0);
        InitActivity.setCurenttotallength(0);
        InitActivity.setCurentvalidtotallength(0);
        Log.d("sinaaaaaa", "startsensor");
        sensorManager = (SensorManager) MainActivity.mainActivity.getSystemService(Context.SENSOR_SERVICE);
        sensor_A = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensor_G = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        //listening for gravity
        gravity = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                for (int i = 0; i < 3; i++) {
                    if (sensorEvent.values[i] != 0) {
                        Gravity[i] = sensorEvent.values[i];
                    } else {
                        Gravity[i] = 1;
                    }
                    //Log.d("sinaaaaaag", "startsensorg" + sensorEvent.values[i]);
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        };
        sensorManager.registerListener(gravity, sensor_G, 1000000 / InitActivity.getFreq());

        //listening for accelerator
        acc = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                for (int i = 0; i < 3; i++) {
                    if (sensorEvent.values[i] != 0) {
                        Acceleretor[i] = sensorEvent.values[i];
                    } else {
                        Acceleretor[i] = 1;
                    }
                    // Log.d("sinaaaaaaacc", "startsensoracc" + sensorEvent.values[i]);
                }

                if (RmsValues.size() <= 1200) {
                    CalculateX();
                }

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        };
        sensorManager.registerListener(acc, sensor_A, 1000000 / InitActivity.getFreq());


    }

    public void CalculateX() {
        ExecutorService executorService = Executors.newFixedThreadPool(6);
        if(lightwork) {
            executorService.execute(new Runnable() {
                public void run() {
                    if (Timerstartvalue <= 1 && Startcheck) {
                        try {


                            //calculate a,b with accelarator
                            double aA = Math.atan(Acceleretor[1] / Acceleretor[2]);
                            double bA = Math.atan(Acceleretor[0] / Math.sqrt(Math.pow(Acceleretor[1], 2) + Math.pow(Acceleretor[2], 2)));
                            //calculate a,b with gravity
                            double aG = Math.atan(Gravity[1] / Gravity[2]);
                            double bG = Math.atan(Gravity[0] / Math.sqrt(Math.pow(Gravity[1], 2) + Math.pow(Gravity[2], 2)));
                            //calculate gzero , azero
                            double gzero = -1 * Math.sin(bG) * Gravity[0] + Math.cos(bG) * Math.sin(aG) * Gravity[1] + Math.cos(bG) * Math.cos(aG) * Gravity[2];
                            double azero = -1 * Math.sin(bA) * Acceleretor[0] + Math.cos(bA) * Math.sin(aA) * Acceleretor[1] + Math.cos(bA) * Math.cos(aA) * Acceleretor[2];
                            double x = azero - gzero;
                            RmsValues.add(x);
                        }catch (Exception e){

                        }
                    }
                }
            });
            lightwork=false;
            executorService.shutdown();
        }else{
            lightwork=true;
        }
    }

    public double CalculateRms() {
        try {
            double sum = 0;
            if (RmsValues != null) {
                for (double value : RmsValues) {
                    sum += Math.pow(value, 2);
                }
                sum /= RmsValues.size();
                sum = (double) Math.sqrt(sum);
                return sum;
            } else {
                return 0;
            }
        }catch(Exception e){
            return 0;
        }
    }

    public void TimerStart() {
        Timerstartvalue = 6;
        //Declare the timer
        if (ts != null && tsstate) {
            ts.cancel();
            tsstate = false;
        }
        ts = new Timer();
        tsk = new TimerTask() {
            Handler h=new Handler();
            @Override
            public void run() {

                tsstate = true;
                if (Timerstartvalue > 1) {
                    Timerstartvalue -= 1;
                    Log.d("insertsina", "timerstart" + Timerstartvalue);
                     h.post(new Runnable() {
                         @Override
                         public void run() {
                             if (HomeFragment.homeFragment != null) { //to check if homefreagment is open!
                                 HomeFragment.homeFragment.start.setText(Timerstartvalue+"");
                             }
                         }
                     });

                } else {
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            if (HomeFragment.homeFragment != null) { //to check if homefreagment is open!
                                HomeFragment.homeFragment.FillUiValue();
                            }
                        }
                    });
                    ts.cancel();
                    tsstate = false;
                }
            }
        };
        //Set the schedule function and rate
        ts.schedule(tsk, 1000, 1000);
    }

   /* public void TimerBetween() {
        Timerbetweenvalue = 5;
        //Declare the timer
        if (tb != null && tbstate) {
            tb.cancel();
            tbstate = false;
        }
        tb = new Timer();
        //Set the schedule function and rate
        tbk = new TimerTask() {
            @Override
            public void run() {
                tbstate = true;
                if (Timerbetweenvalue > 0) {
                    Timerbetweenvalue -= 1;
                    Log.d("insertsina", "timerbet" + Timerbetweenvalue);
                } else {
                    tb.cancel();
                    tbstate = false;
                }
            }
        };
        tb.schedule(tbk, 1000, 1000);

    }*/

    public void EndDataPacket() {
        //complete data fileds
        try {


            if (l1 != null && l2 != null) {
                CheckValid = true;
                dataPacket.setDatetime(MyUtiles.GetDate() + " " + MyUtiles.GetTime());
                dataPacket.setAveragevelocity((l1.getSpeed() + l2.getSpeed()) * 1.8f);
                InitActivity.setCurentavgspeed(dataPacket.getAveragevelocity());
                Log.d("speeeeed",InitActivity.getCurentavgspeed()+"");
                AllAverageSpeed += dataPacket.getAveragevelocity();
                dataPacket.setRms((float) CalculateRms());
                dataPacket.setLength(l1.distanceTo(l2));
                AllLength += dataPacket.getLength();
                InitActivity.setCurenttotallength(AllLength);
                completePacket.getGpsdata().add(dataPacket);
                if (Math.max(l1.getSpeed(), l2.getSpeed()) * 3.6f > AllMaxSpeed) { // calculate for the maxspeed in completedata
                    AllMaxSpeed = Math.max(l1.getSpeed(), l2.getSpeed()) * 3.6f;
                    InitActivity.setCurentmaxspeed(AllMaxSpeed);
                    Log.d("speeeeed",InitActivity.getCurentmaxspeed()+"");
                }
                if (dataPacket.getAveragevelocity() >= 20) {
                    AllValidLength += dataPacket.getLength();
                    InitActivity.setCurentvalidtotallength(AllValidLength);
                }
            }
        }catch (Exception e){
            
        }
        //reset for capture new data
        RmsValues = new ArrayList<>();
        dataPacket = new DataPacket();
        firstlocation = true;
        l1 = null;
        l2 = null;

    }

    public void LocationGetting(Location location) {
        try {
            //get locations and set into mainactivity class!
            if (location != null) {
                InitActivity.setCurentspeed(location.getSpeed() * 3.6f);
                if (HomeFragment.homeFragment != null && Timerstartvalue <= 1) { //to check if homefreagment is open!
                    HomeFragment.homeFragment.FillUiValue();
                }
                if (Timerstartvalue <= 1 && Startcheck) { // has time left from 5 starting second
                    // if (Timerbetweenvalue > 0) { //has time remained from 5 second between 2 data
                    if (firstlocation) {
                        l1 = location;
                        dataPacket.SetLocation1(location);
                        firstlocation = false;
                        Log.d("insertsina", "loc1 add");
                        //TimerBetween();
                    } else {
                        l2 = location;
                        dataPacket.SetLocation2(location); // get second location
                        //TimerBetween();
                        Log.d("insertsina", "loc2 add");
                        EndDataPacket();
                    }
                    //} else {
                    //   Log.d("insertsina", "between time");
                    //  l1 = null;
                    //  l2 = null;
                    //  firstlocation = true;
                    //  RmsValues = new ArrayList<>();
//                }
                } else {
                    Log.d("insertsina", "first time");
                }
            }
        }catch (Exception e){

        }
    }

}

