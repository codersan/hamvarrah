package com.ace.roadstateevaluator.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ace.roadstateevaluator.CollectData;
import com.ace.roadstateevaluator.GpsService;
import com.ace.roadstateevaluator.GsonModels.CompletePacket;
import com.ace.roadstateevaluator.GsonModels.DataPacket;
import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.GsonModels.TakeSession;
import com.ace.roadstateevaluator.GsonModels.User;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.Vars;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static GpsService gpsService;
    public static FragmentManager fragmentManager;
    public static MainActivity mainActivity;
    public NavController navController;
    public static Intent intent;
    AppBarConfiguration appBarConfiguration;
    public boolean should_this_shit_be_back_button=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL); too rtl all
        //--------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ViewCompat.setLayoutDirection(toolbar, ViewCompat.LAYOUT_DIRECTION_RTL); //to change burger menu icon to right

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Gravity.END or Gravity.RIGHT
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                } else {
                    drawer.openDrawer(Gravity.RIGHT);
                }
            }
        });
        //--------------------------------------
        mainActivity=this;

        if (GetUserCarsList.getCars().size() == 0 ||GetUserCarsList.getCars()==null) {
            MyUtiles.ReadCarDb(this);
        }


        fragmentManager=getSupportFragmentManager();

        CollectData collectData= CollectData.GetInstance();
        GpsService d = new GpsService();
        intent = new Intent(this, GpsService.class);
        Log.d("Sloooc","intent");
        this.getApplication().startService(intent);




        //Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //DrawerLayout drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);




        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //for each id of item do some!

                navController.navigate(item.getItemId());



                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }
        });

    }

    public void backpressed(View view){

        onBackPressed();
    }


    @Override
    public void onBackPressed() {

        if (getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment).getChildFragmentManager().getBackStackEntryCount()==0){
            //at home ready to exit


            show_exit_dialog();

        }else {

            super.onBackPressed();
        }


    }

    private void show_exit_dialog() {

        final Dialog dialog = new Dialog(this,R.style.exitDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(this);
        final View mainview = inflater.inflate(R.layout.exit_dialog, null);
        dialog.setContentView(mainview);


        Button full_exit = mainview.findViewById(R.id.b_exit_full);
        Button background_exit = mainview.findViewById(R.id.b_exit_back);

        full_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //GpsService.GetInstance().stopForeground(true); should fun from applicition not activity
                InitActivity.setCurentspeed(0f);
                InitActivity.setCurentvalidtotallength(0f);
                InitActivity.setCurenttotallength(0f);
                //InitActivity.setCurentavgspeed(0f);
               //InitActivity.setCurentmaxspeed(0f);
                Toast.makeText(MainActivity.this,"با تشکر از همکاری شما",Toast.LENGTH_SHORT).show();
                GpsService.GetInstance().Stop();
                Log.d("Sloooc","intent");
                MainActivity.this.getApplication().stopService(intent);
                dialog.dismiss();
                finish();
            }
        });
        background_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                dialog.dismiss();
                startActivity(startMain);
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (Vars.width * 0.9);
        lp.height = (int) (Vars.height * 0.5);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.backdialoge));





        dialog.show();








    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        final DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (item != null && item.getItemId()== android.R.id.home) {
            if (!should_this_shit_be_back_button) {

                if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                }
            }else {

                onBackPressed();
            }
        }
        return true;

    }

    public void show_tuto(View view){

        navController.navigate(R.id.navigation_tuto2);
//        Toast.makeText(mainActivity, "clcied", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }

}