package com.ace.roadstateevaluator.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.Vars;
import com.ace.roadstateevaluator.fragments.login.LoginFragment;
import com.ace.roadstateevaluator.fragments.login.Signup2Fragment;
import com.ace.roadstateevaluator.fragments.login.SingUpFragment;

public class LoginActivity extends AppCompatActivity {

    public String[] uservalues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, LoginFragment.newInstance(this))
                    .commitNow();
        }

    }


    public void go_signup() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enterfromdown, R.anim.exitfadeout, R.anim.enterfadein, R.anim.exitfromdown); //enter new and exit current anim!
        transaction.replace(R.id.container, SingUpFragment.newInstance(this));
        transaction.addToBackStack("login");
        transaction.commit();

    }

    public void go_signup2(int type) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.enterfromright, R.anim.exitfromleft, R.anim.enterfromleft, R.anim.exitfromright); //enter new and exit current anim!
        transaction.replace(R.id.container, Signup2Fragment.newInstance(type, this));
        transaction.addToBackStack("sign1");
        transaction.commit();

    }


    public void show_rules(View view){
        //create new car
        final Dialog dialog = new Dialog(this,R.style.PauseDialog);
        LayoutInflater inflater = LayoutInflater.from(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final View mainview = inflater.inflate(R.layout.showrules, null);
        dialog.setContentView(mainview);



        Button b=mainview.findViewById(R.id.b_rules);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        TextView tv=mainview.findViewById(R.id.tv_rules);

        tv.setMovementMethod(new ScrollingMovementMethod());



//        //make dialog BIG
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = (int) (Vars.width );
        lp.height = (int) (Vars.height * 0.8);
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.backdialoge));
//



        //
        Window window = dialog.getWindow();
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);



        dialog.show();
    }

    public void backpressed(View view) {
        onBackPressed();
    }
}