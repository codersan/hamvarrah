package com.ace.roadstateevaluator.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.ace.roadstateevaluator.GsonModels.GetUserCarsList;
import com.ace.roadstateevaluator.MyUtiles;
import com.ace.roadstateevaluator.R;
import com.ace.roadstateevaluator.Vars;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class InitActivity extends AppCompatActivity {
    private static String Username=""; // like cina_pm
    private static String Password=""; // like cina_pm
    private static String UserType="";
    private static String UserJob="";
    private static String UserPhone="";
    private static String UserEmail="";
    public static int UserTypeIndex=-1;
    private static String TokenAccess="";
    private static String TokenRefresh="";
    private static String UserFamilyName=""; //like sina moradi
    private static String UserFirstName=""; //like sina moradi
    private static int UserId;
    private static int CarId;
    private static int Freq;
    private static int SqliteInt;
    //--------------------------------
    private static float totallength;
    private static float validtotallength;
    private static float curenttotallength;
    private static float curentvalidtotallength;
    private static float curentspeed;
    private static float curentmaxspeed;
    private static float curentavgspeed;
    private static float avgspeed;
    private static float maxspeed;
    private static String freqspeed;
    //--------------------------------
    private static int student_user_id_id;
    private static String student_university;
    private static String student_level;
    private static String student_supervisor;
    private static String student_topic;
    private static String student_target;
    //------------------------------
    private static int professor_user_id_id;
    private static String professor_university;
    private static String professor_degree;
    private static String professor_topic;
    private static String professor_target;
    //--------------------------------
    private static int project_owner_user_id_id;
    private static String project_name;
    private static String project_start_date;
    private static int project_organizations_id;
    private static String project_user_role;
    private static float project_km;
    //-----------------------------------
    SharedPreferences pref;
    static SharedPreferences.Editor e;
    ImageView imageinit;
    //-------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        pref = this.getSharedPreferences("Init",MODE_PRIVATE); // private maens just in this app you have access to this file!
        e = pref.edit();
        Vars.calculate_size(getWindowManager());
         imageinit = findViewById(R.id.imginit);


        Readsharedpref();

        checkPermissions();
    }


    private void checkPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED

            ) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 69);
                return;
            }
        }else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            ) {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 69);


                return;
            }
        }
        go();
    }

    public void go(){
        final Handler handler = new Handler(Looper.getMainLooper()); //
        Runnable runnable = new Runnable() {
            public void run() {

                if (Username.equals("null")) {
                    Intent login = new Intent(InitActivity.this, LoginActivity.class);
                    Pair[] pairs = new Pair[1];
                    pairs[0]=new Pair<View,String>(imageinit,"imaget");
                    ActivityOptions options=ActivityOptions.makeSceneTransitionAnimation(InitActivity.this,pairs);
                    startActivity(login,options.toBundle());
                }else {
                    startActivity(new Intent(InitActivity.this, MainActivity.class));

                }
                Runnable runnable2 = new Runnable() {
                    public void run() {

                      finish();
                        // startActivity(new Intent(InitActivity.this, MainActivity.class));//for now test
                    }
                };
                handler.postDelayed(runnable2, 1000);
            }
        };
        handler.postDelayed(runnable, 3000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 69) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED
                        && grantResults[4] == PackageManager.PERMISSION_GRANTED) {
//                request is granted
                    go();
                } else {
                    Toast.makeText(this, "دسترسی مجوزهارا انجام دهید", Toast.LENGTH_SHORT).show();
                    finish();
//                request is denied
                }
            }else {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED
                        && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                    go();
//                request is granted
                } else {
                    Toast.makeText(this, "دسترسی مجوزهارا انجام دهید", Toast.LENGTH_SHORT).show();
                    finish();
//                request is denied
                }
            }

        }

    }
    @Override
    public void onBackPressed(){
        finish();
    }
    private void Readsharedpref() {
        setUsername(pref.getString("UserName", "null"));//if username is null go for signin and signup
        setUserType(pref.getString("UserType", "null"));// second paramter return when notfound key of xml!
        setUserJob(pref.getString("UserJob", "null"));
        setPassword(pref.getString("Password", "null"));
        setUserFamilyName(pref.getString("UserFamilyName", "null"));
        setUserFirstName(pref.getString("UserFirstName", "null"));
        setUserPhone(pref.getString("UserPhone", "null"));
        setUserEmail(pref.getString("UserEmail", "null"));
        setTokenAccess(pref.getString("TokenAccess", "null"));
        setTokenRefresh(pref.getString("TokenRefresh", "null"));
        setCarId(pref.getInt("CarId", 0));
        setUserId(pref.getInt("UserId", 0));
        setFreq(pref.getInt("Freq", 100));
        setSqliteInt(pref.getInt("SqliteInt", 1));
        //--------------------------------
        setTotallength(pref.getFloat("TotalLength", 0));
        setValidtotallength(pref.getFloat("ValidTotalLength", 0));
        setCurenttotallength(pref.getFloat("CurTotalLength", 0));
        setCurentvalidtotallength(pref.getFloat("CurValidTotalLength", 0));
        setCurentmaxspeed(pref.getFloat("CurMaxSpeed", 0));
        setCurentavgspeed(pref.getFloat("CurAvgSpeed", 0));
        setMaxspeed(pref.getFloat("MaxSpeed", 0));
        setAvgspeed(pref.getFloat("AvgSpeed", 0));
        setFreqspeed(pref.getString("FreqSpeed", "خالی"));
        setCurentspeed(pref.getFloat("CurSpeed", 0));
        //--------------------------------
        setStudent_user_id_id(pref.getInt("StudentId", 0));
        setStudent_university(pref.getString("StudentUni", "null"));
        setStudent_level(pref.getString("StudentLevel", "null"));
        setStudent_supervisor(pref.getString("StudentSuper", "null"));
        setStudent_topic(pref.getString("StudentTopic", "null"));
        setStudent_target(pref.getString("StudentTarget", "null"));
        //--------------------------------
        setProfessor_user_id_id(pref.getInt("ProfessorId", 0));
        setProfessor_university(pref.getString("ProfessorUni", "null"));
        setProfessor_degree(pref.getString("ProfessorDeg", "null"));
        setProfessor_topic(pref.getString("ProfessorTopic", "null"));
        setProfessor_target(pref.getString("ProfessorTarget", "null"));
        //--------------------------------
        setProject_owner_user_id_id(pref.getInt("ProjectId", 0));
        setProject_name(pref.getString("ProjectName", "null"));
        setProject_start_date(pref.getString("ProjectStartDate", ""));
        setProject_organizations_id(pref.getInt("ProjectOrganId", 0));
        setProject_user_role(pref.getString("ProjectUSerRole", "null"));
        setProject_km(pref.getFloat("ProjectKm", 0));
    }

    public static void setUsername(String username) {
        Username = username;
        e.putString("UserName", username);
        e.apply();
    }

    public static void setFreq(int freq) {
        Freq = freq;
        e.putInt("Freq", freq);
        e.apply();
    }

    public static void setUserType(String userType) {
        UserType = userType;
        e.putString("UserType", userType);
        e.apply();


        switch (userType){
            case "regular":
                UserTypeIndex=0;
                break;
            case "student":
                UserTypeIndex=1;
                break;
            case "professor":
                UserTypeIndex=2;
                break;
            case "project":
                UserTypeIndex=3;
                break;
        }


    }
    public static void setUserId(int userId) {
        UserId = userId;
        e.putInt("UserId", userId);
        e.apply();
    }

    public static void setUserJob(String userJob) {
        UserJob = userJob;
        e.putString("UserJob", userJob);
        e.apply();
    }

    public static void setPassword(String password) {
        Password = password;
        e.putString("Password", password);
        e.apply();
    }

    public static void setCarId(int carId) {
        CarId = carId;
        e.putInt("CarId", carId);
        e.apply();
    }

    public static void setSqliteInt(int sqliteInt) {
        SqliteInt = sqliteInt;
        e.putInt("SqliteInt", sqliteInt);
        e.apply();
    }

    public static void setUserPhone(String userPhone) {
        UserPhone = userPhone;
        e.putString("UserPhone", userPhone);
        e.apply();
    }

    public static void setTokenAccess(String tokenAccess) {
        TokenAccess = tokenAccess;
        e.putString("TokenAccess", tokenAccess);
        e.apply();
    }

    public static void setTokenRefresh(String tokenRefresh) {
        TokenRefresh = tokenRefresh;
        e.putString("TokenRefresh", tokenRefresh);
        e.apply();
    }

    public static void setUserEmail(String userEmail) {
        UserEmail = userEmail;
        e.putString("UserEmail", userEmail);
        e.apply();
    }

    public static void setUserFirstName(String userFirstName) {
        UserFirstName = userFirstName;
        e.putString("UserFirstName", userFirstName);
        e.apply();
    }

    public static void setUserFamilyName(String userFamilyName) {
        UserFamilyName = userFamilyName;
        e.putString("UserFamilyName", userFamilyName);
        e.apply();
    }

    public static void setStudent_user_id_id(int student_user_id_id) {
        InitActivity.student_user_id_id = student_user_id_id;
        e.putInt("StudentId", student_user_id_id);
        e.apply();
    }

    public static void setStudent_university(String student_university) {
        InitActivity.student_university = student_university;
        e.putString("StudentUni", student_university);
        e.apply();
    }

    public static void setStudent_level(String student_level) {
        InitActivity.student_level = student_level;
        e.putString("StudentLevel", student_level);
        e.apply();
    }

    public static void setStudent_supervisor(String student_supervisor) {
        InitActivity.student_supervisor = student_supervisor;
        e.putString("StudentSuper", student_supervisor);
        e.apply();
    }

    public static void setStudent_topic(String student_topic) {
        InitActivity.student_topic = student_topic;
        e.putString("StudentTopic", student_topic);
        e.apply();
    }

    public static void setStudent_target(String student_target) {
        InitActivity.student_target = student_target;
        e.putString("StudentTarget", student_target);
        e.apply();
    }

    public static void setProfessor_user_id_id(int professor_user_id_id) {
        InitActivity.professor_user_id_id = professor_user_id_id;
        e.putInt("ProfessorId", professor_user_id_id);
        e.apply();
    }

    public static void setProfessor_university(String professor_university) {
        InitActivity.professor_university = professor_university;
        e.putString("ProfessorUni", professor_university);
        e.apply();
    }

    public static void setProfessor_degree(String professor_degree) {
        InitActivity.professor_degree = professor_degree;
        e.putString("ProfessorDeg", professor_degree);
        e.apply();
    }

    public static void setProfessor_topic(String professor_topic) {
        InitActivity.professor_topic = professor_topic;
        e.putString("ProfessorTopic", professor_topic);
        e.apply();
    }

    public static void setProfessor_target(String professor_target) {
        InitActivity.professor_target = professor_target;
        e.putString("ProfessorTarget", professor_target);
        e.apply();
    }

    public static void setProject_owner_user_id_id(int project_owner_user_id_id) {
        InitActivity.project_owner_user_id_id = project_owner_user_id_id;
        e.putInt("ProjectId", project_owner_user_id_id);
        e.apply();
    }

    public static void setProject_name(String project_name) {
        InitActivity.project_name = project_name;
        e.putString("ProjectName", project_name);
        e.apply();
    }

    public static void setProject_start_date(String project_start_date) {
        InitActivity.project_start_date = project_start_date;
        e.putString("ProjectStartDate", project_start_date);
        e.apply();
    }

    public static void setProject_organizations_id(int project_organizations_id) {
        InitActivity.project_organizations_id = project_organizations_id;
        e.putInt("ProjectOrganId", project_organizations_id);
        e.apply();
    }

    public static void setProject_user_role(String project_user_role) {
        InitActivity.project_user_role = project_user_role;
        e.putString("ProjectUSerRole", project_user_role);
        e.apply();
    }

    public static void setProject_km(float project_km) {
        InitActivity.project_km = project_km;
        e.putFloat("ProjectKm", project_km);
        e.apply();
    }

    public static void setTotallength(float totallength) {
        InitActivity.totallength = totallength;
        e.putFloat("TotalLength", totallength);
        e.apply();
    }

    public static void setValidtotallength(float validtotallength) {
        InitActivity.validtotallength = validtotallength;
        e.putFloat("ValidTotalLength", validtotallength);
        e.apply();
    }

    public static void setCurenttotallength(float curenttotallength) {
        InitActivity.curenttotallength = curenttotallength;
        e.putFloat("CurTotalLength", curenttotallength);
        e.apply();
    }

    public static void setCurentspeed(float curentspeed) {
        InitActivity.curentspeed = curentspeed;
        e.putFloat("CurSpeed", curentspeed);
        e.apply();
    }

    public static void setCurentvalidtotallength(float curentvalidtotallength) {
        InitActivity.curentvalidtotallength = curentvalidtotallength;
        e.putFloat("CurValidTotalLength", curentvalidtotallength);
        e.apply();
    }

    public static void setAvgspeed(float avgspeed) {
        InitActivity.avgspeed = avgspeed;
        e.putFloat("AvgSpeed", avgspeed);
        e.apply();
    }

    public static void setMaxspeed(float maxspeed) {
        InitActivity.maxspeed = maxspeed;
        e.putFloat("MaxSpeed", maxspeed);
        e.apply();
    }

    public static void setFreqspeed(String freqspeed) {
        InitActivity.freqspeed = freqspeed;
        e.putString("FreqSpeed", freqspeed);
        e.apply();
    }

    public static void setCurentmaxspeed(float curentmaxspeed) {
        InitActivity.curentmaxspeed = curentmaxspeed;
        e.putFloat("CurMaxSpeed", curentmaxspeed);
        e.apply();
    }

    public static void setCurentavgspeed(float curentavgspeed) {
        InitActivity.curentavgspeed = curentavgspeed;
        e.putFloat("CurAvgSpeed", curentavgspeed);
        e.apply();
    }

    public static float getCurentmaxspeed() {
        return curentmaxspeed;
    }

    public static float getCurentavgspeed() {
        return curentavgspeed;
    }

    public static String getUserEmail() {
        return UserEmail;
    }

    public static String getUserFamilyName() {
        return UserFamilyName;
    }
    public static String getUserPhone() {
        return UserPhone;
    }
    public static String getUsername() {
        return Username;
    }

    public static String getUserType() {
        return UserType;
    }

    public static String getUserTypePersian() {
        //give me persian
        return MyUtiles.USER_TYPE_e2p.get(UserType);
    }

    public static float getCurentspeed() {
        return curentspeed;
    }

    public static int getUserId() {
        return UserId;
    }
    public static int getCarId() {
        return CarId;
    }

    public static float getTotallength() {
        return totallength;
    }

    public static float getValidtotallength() {
        return validtotallength;
    }

    public static float getCurenttotallength() {
        return curenttotallength;
    }

    public static float getCurentvalidtotallength() {
        return curentvalidtotallength;
    }

    public static float getAvgspeed() {
        return avgspeed;
    }

    public static float getMaxspeed() {
        return maxspeed;
    }

    public static String getFreqspeed() {
        return freqspeed;
    }

    public static int getFreq() {
        return Freq;
    }
    public static int getSqliteInt() {
        SqliteInt+=1;
        setSqliteInt(SqliteInt);
        return SqliteInt;

    }
    public static String getTokenAccess() {
        return TokenAccess;
    }

    public static String getTokenRefresh() {
        return TokenRefresh;
    }

    public static int getStudent_user_id_id() {
        return student_user_id_id;
    }

    public static String getStudent_university() {
        return student_university;
    }

    public static String getStudent_level() {
        return student_level;
    }

    public static String getStudent_supervisor() {
        return student_supervisor;
    }

    public static String getStudent_topic() {
        return student_topic;
    }

    public static String getStudent_target() {
        return student_target;
    }

    public static int getProfessor_user_id_id() {
        return professor_user_id_id;
    }

    public static String getProfessor_university() {
        return professor_university;
    }

    public static String getProfessor_degree() {
        return professor_degree;
    }

    public static String getProfessor_topic() {
        return professor_topic;
    }

    public static String getProfessor_target() {
        return professor_target;
    }

    public static int getProject_owner_user_id_id() {
        return project_owner_user_id_id;
    }

    public static String getProject_name() {
        return project_name;
    }

    public static String getProject_start_date() {
        return project_start_date;
    }

    public static int getProject_organizations_id() {
        return project_organizations_id;
    }

    public static String getProject_user_role() {
        return project_user_role;
    }

    public static float getProject_km() {
        return project_km;
    }

    public static String getPassword() {
        return Password;
    }

    public static String getUserFirstName() {
        return UserFirstName;
    }

    public static String getUserJob() {
        return UserJob;
    }
}